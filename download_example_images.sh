#!/bin/bash


flight_to_download="http://breakout.hs-augsburg.de/searchwing-gui/2022-10-01T13-31-59-maggy-BurrianaSea-ImageGathering" 
first_n_files_to_download=50
download_size=$(( 2*first_n_files_to_download))

echo "Downloading example data. About ${download_size}MB of data."
echo "Wait 5 sec before start."

sleep 5


script_dir=$(dirname "$(realpath "$0")")

download_files () {
    url=$1        # HTTP address
    folder=$2     # Sub-folder name
    file_count=$3 # Count of files to download

    # Extract the flight timestamp from the URL
    timestamp=$(basename "$url")

    # Define the data directory based on the timestamp
    data_dir=$script_dir/backend/data/$timestamp/

    # Create the directory if it doesn't exist
    mkdir -p "$data_dir"

    # Download the first $file_count files (including .jpg) from the specified URL and folder
    wget -q -O - "$url/$folder/" | \
    grep -oP '(?<=href=")[^"]+' | \
    awk 'NR <= '$file_count | \
    sed 's|^|'"$url/$folder/"'|' | \
    wget -P "$data_dir$folder/" -nc -i -
}

download_files $flight_to_download "cam-l" $first_n_files_to_download
download_files $flight_to_download "cam-r" $first_n_files_to_download

echo "Finished, thank you for your patience"
