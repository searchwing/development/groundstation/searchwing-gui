const util = require('util')
const fs = require('fs')
const fast_exif = require('fast-exif')

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)

const makeEmptyMetadata = () => ({
  manual: { views: 0, flagged: false, annotations: [] },
  algoResults: {},
  img: false,
  exif: null
})

function sortByPrefix (files) {
  const files_by_prefix = {}
  files.forEach(file => {
    const parts = file.split('.jpg')
    if (parts.length === 2) {
      const [basename, rest] = parts
      const rest_parts = rest.split('.')
      // console.log(file, parts, basename, rest, rest_parts)
      if (rest === '') {
        // image file
        const prefix = basename + '.jpg'
        if (!files_by_prefix.hasOwnProperty(prefix)) {
          files_by_prefix[prefix] = {}
        }
        files_by_prefix[prefix].img = file
      } else if (rest_parts.length >= 2) {
        // json files
        const prefix = basename + '.jpg'
        if (!files_by_prefix.hasOwnProperty(prefix)) {
          files_by_prefix[prefix] = {}
        }
        if (rest_parts.length === 2 && rest_parts[1] === 'json') {
          files_by_prefix[prefix].manual = file
        }
        if (rest_parts.length === 3 && rest_parts[2] === 'json') {
          if (!files_by_prefix[prefix].hasOwnProperty('algo')) {
            files_by_prefix[prefix].algo = {}
          }
          files_by_prefix[prefix].algo[rest_parts[1]] = file
        }
      }
    }
  })
  return files_by_prefix
}

function degrees_to_float (ds) {
  return ds[0] + ds[1] / 60 + ds[2] / 3600
}

function simplifyGPS (exif) {
  let lat = degrees_to_float(exif.gps.GPSLatitude)
  let lon = degrees_to_float(exif.gps.GPSLongitude)

  if (exif.gps.GPSLatitudeRef == 'S') {
    lat = lat * -1
  }
  if (exif.gps.GPSLongitudeRef == 'W') {
    lon = lon * -1
  }
  exif.gps.simplifiedLatLon = {
    lat: lat.toFixed(6) + 'N',
    lon: lon.toFixed(6) + 'E',
    alt:
      ((exif &&
        exif.gps &&
        exif.gps.GPSAltitude &&
        exif.gps.GPSAltitude.toFixed(1)) ||
        '?') + 'm'
  }
}

function simplifyUserComment (exif) {
  try {
    if (exif.exif.UserComment && isJsonFormat(exif.exif.UserComment)) {
      var UserComment = exif.exif.UserComment.toString("ascii")
      UserComment = JSON.parse(UserComment.slice(8))
      UserComment.imgcrns.lowerLeft = JSON.parse("[" + UserComment.imgcrns.ll + "]").slice(0, -1)
      UserComment.imgcrns.lowerRight= JSON.parse("[" + UserComment.imgcrns.lr + "]").slice(0, -1)
      UserComment.imgcrns.upperLeft = JSON.parse("[" + UserComment.imgcrns.ul + "]").slice(0, -1)
      UserComment.imgcrns.upperRight = JSON.parse("[" + UserComment.imgcrns.ur + "]").slice(0, -1)
      exif.exif.UserCommentSimplified = UserComment
    }
  } catch (err) {
    console.error("Could not simplify UserComment (so in particular the lat/lon of the image corners) for image:")
    console.error(exif.exif)
    }
}

function isJsonFormat(UserComment){
  if (UserComment.toString("ascii").slice(0,5) === "ASCII")
  {
    return true
  }
  return false
}


async function readExif (fullPath) {
  let exif = (await fast_exif.read(fullPath)) || {}
  if (exif.hasOwnProperty('gps')) {
    simplifyGPS(exif)
  }
  simplifyUserComment(exif)
  try {
    delete exif.exif.MakerNote
  } catch (err) {}
  return exif
}

class OneSideMetadataManager {
  constructor (datadir, subfolder, side, config) {
    this.datadir = datadir
    this.subfolder = subfolder
    this.side = side
    this.config = config
    this.files = {}
    this.isBusy = false
  }

  reset () {
    if (this.isBusy) {
      setTimeout(() => this.reset(), 100)
    } else {
      this.files = {}
    }
  }

  async loadManual (prefix) {
    let data = null
    try {
      data = JSON.parse(
        await readFile(
          `${this.datadir}/${this.subfolder}/${
            this.config.left_right_folder_names[this.side]
          }/${prefix}.json`
        )
      )
    } catch (err) {
      console.error(err)
    }
    if (data) {
      try {
        if (data.hasOwnProperty('manual')) {
          if (
            data.manual.hasOwnProperty('views') &&
            typeof data.manual.views === 'object'
          ) {
            // Remove old views struct with different view types
            delete data.manual.views
          }
          // Merge generated empty struct with loaded one. That ensures that when loading an old json file, new attributes will still be present
          this.files[prefix].manual = {
            ...this.files[prefix].manual,
            ...data.manual
          }
        }
        // // Activate this to load exif from manual json file, also change the lines where loadExif and saveManual are called for this behavior
        // if (data.hasOwnProperty('exif')) {
        //   this.files[prefix].exif = data.exif
        // }
      } catch (err) {
        console.error(err)
      }
    }
  }

  async loadExif (prefix) {
    let exif = null
    try {
      exif = await readExif(
        `${this.datadir}/${this.subfolder}/${
          this.config.left_right_folder_names[this.side]
        }/${prefix}`
      )
    } catch (err) {
      console.error(err)
    }
    if (exif) {
      try {
        this.files[prefix].exif = exif
      } catch (err) {}
    }
  }

  async loadAlgoResults (prefix, algo) {
    let data = null
    try {
      data = JSON.parse(
        await readFile(
          `${this.datadir}/${this.subfolder}/${
            this.config.left_right_folder_names[this.side]
          }/${prefix}.${algo}.json`
        )
      )
    } catch (err) {
      console.error(err)
    }
    if (data) {
      try {
        this.files[prefix].algoResults[algo] = data
      } catch (err) {}
    }
  }

  async saveManual (prefix, data) {
    if (this.files.hasOwnProperty(prefix)) {
      const data = {
        manual: this.files[prefix].manual
        // exif: this.files[prefix].exif
      }
      writeFile(
        `${this.datadir}/${this.subfolder}/${
          this.config.left_right_folder_names[this.side]
        }/${prefix}.json`,
        JSON.stringify(data)
      ).catch(err => console.error(err))
    }
  }

  async onSubfolderFilesUpdate (files) {
    const newFilePrefixes = []
    const newAlgoPrefixes = []
    const removedFilePrefixes = []
    this.isBusy = true
    try {
      // console.log(
      //   'OneSideMetadataManager.onSubfolderFilesUpdate',
      //   this.subfolder,
      //   this.side,
      //   files.length
      // )
      Object.entries(this.files).forEach(([prefix, _]) => {
        if (files.indexOf(prefix) === -1) {
          // Image file is one of the removed
          // TODO: what if e.g. only the metadata file or an algo file is removed?
          delete this.files[prefix]
          // this.files[prefix].img = false
          removedFilePrefixes.push(prefix)
        }
      })

      const files_by_prefix = sortByPrefix(files)
      // console.log(files_by_prefix)
      for (const [file, struct] of Object.entries(files_by_prefix)) {
        if (struct.img) {
          if (!this.files.hasOwnProperty(file)) {
            // new image
            this.files[file] = makeEmptyMetadata()
            this.files[file].img = true
            newFilePrefixes.push(file)
            if (struct.manual) {
              // The manual file is only loaded if this is a new image (we do not need to watch for changes of that file, as only we write to it)
              await this.loadManual(file)
            } else {
              // If we put the await this.loadExif(file) here, then it will not be loaded if there is a manual annotation file
              // (If there is no exif data present and it will not be loaded right now from an existing json => load it from jpg)
            }
            await this.loadExif(file)
          }
          if (struct.algo) {
            for (const algo of Object.keys(struct.algo)) {
              await this.loadAlgoResults(file, algo)
              newAlgoPrefixes.push(file)
            }
          }
        }
      }
      // console.log(this.files)
    } catch (err) {
      console.error(err)
    }
    this.isBusy = false
    return [newFilePrefixes, newAlgoPrefixes, removedFilePrefixes]
  }

  getDataByPrefixes (prefixes) {
    const data = {}
    prefixes
      .filter(prefix => this.files.hasOwnProperty(prefix))
      .forEach(prefix => (data[prefix] = this.files[prefix]))
    return data
  }

  updateMetadata (msg) {
    try {
      if (msg.file && this.files.hasOwnProperty(msg.file)) {
        if (msg.type === 'set_viewed') {
          this.files[msg.file].manual.views += 1
          this.saveManual(msg.file)
          return this.files[msg.file].manual
        } else if (msg.type === 'toggle_flagged') {
          this.files[msg.file].manual.flagged = !this.files[msg.file].manual
            .flagged
          this.saveManual(msg.file)
          return this.files[msg.file].manual
        }
      }
    } catch (err) {
      console.error(
        `Error updating ${msg.subfolder}/${
          this.config.left_right_folder_names[msg.side]
        }/${msg.file} (msg.type=${msg.type})`
      )
      console.error(err)
    }
    return false
  }
}

class MetadataManager {
  constructor (datadir, subfolder, config) {
    this.datadir = datadir
    this.subfolder = subfolder
    this.metadataManagers = {
      left: new OneSideMetadataManager(
        this.datadir,
        this.subfolder,
        'left',
        config
      ),
      right: new OneSideMetadataManager(
        this.datadir,
        this.subfolder,
        'right',
        config
      )
    }
  }

  reset () {
    this.metadataManagers.left.reset()
    this.metadataManagers.right.reset()
  }

  getDataByPrefixes (side, prefixes) {
    return this.metadataManagers[side].getDataByPrefixes(prefixes)
  }

  async onSubfolderFilesUpdate (side, files) {
    return await this.metadataManagers[side].onSubfolderFilesUpdate(files)
  }

  updateMetadata (msg) {
    if (msg.side && this.metadataManagers[msg.side]) {
      return this.metadataManagers[msg.side].updateMetadata(msg)
    }
    return false
  }
}

module.exports = { MetadataManager }
