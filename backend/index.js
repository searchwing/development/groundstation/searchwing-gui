// const chokidar = require('chokidar')
const path = require('path')
const util = require('util')
const WebSocket = require('ws')
const express = require('express')
const fs = require('fs')
const { RootFolderWatcher } = require('./folderWatcher.js')
const { WhoAndWhat } = require('./who_and_what.js')
const { Exporter } = require('./export.js')
const config = require('./config.js')
config.datadir = config.datadir || `${__dirname}/data`
console.log('Config:', util.inspect(config, { depth: Infinity, colors: true }))

const wss = new WebSocket.Server({ port: 3001 })
const app = express()
const port = 3002

const waw = new WhoAndWhat(config)

// ========================= HTTP Server =========================
app.get('/image/:subfolder/:side/:file', (req, res) => {
  res.sendFile(
    path.join(
      config.datadir,
      `./${req.params.subfolder}/${
        config.left_right_folder_names[req.params.side]
      }/${req.params.file}`
    )
  )
})

app.post('/export/flagged/:subfolder', async (req, res) => {
  exporter = new Exporter(config, req.params.subfolder)
  await exporter.exportFlagged()
  res.send('{}')
})

app.listen(port, () => console.log(`Server listening on port ${port}!`))

// ========================= WebSockets =========================

let sockets = new Set()

wss.on('connection', ws => {
  sockets.add(ws)

  let msg = JSON.stringify({
    type: 'subfolders',
    subfolders: rootfolder_watcher.getSubfoldersHomeScreen()
  })
  ws.send(msg)

  msg = JSON.stringify({
    type: 'frontendSettings',
    settings: config.frontendSettings
  })

  ws.send(msg)

  ws.on('message', msg => {
    try {
      msg = JSON.parse(msg)
      if (msg.hasOwnProperty('type')) {
        if (msg.type === 'watch_subfolder') {
          waw.socketWatchesSubfolder(ws, msg.subfolder)
        } else if (msg.type === 'unwatch_subfolder') {
          waw.unsubscribeSocket(ws)
        } else if (msg.type === 'set_viewed') {
          waw.updateMetadata(msg)
        } else if (msg.type === 'toggle_flagged') {
          waw.updateMetadata(msg)
        } else if (msg.type === 'set_flagged') {
          waw.updateMetadata(msg)
        }
      } else {
        console.error(`Websocket message ${msg} has no field 'type'`)
      }
    } catch (err) {
      console.error(err)
    }
  })
  ws.on('close', () => {
    sockets.delete(ws)
    waw.socketDisconnected(ws)
  })
})

function broadcast (str) {
  // console.log('broadcast', str)
  sockets.forEach(ws => ws.send(str))
}

// ========================= Watch data folder =========================

const rootfolder_watcher = new RootFolderWatcher(config, config.datadir)
rootfolder_watcher.onChangedSubfolders = (subfolders,subfoldersText) => {
  let msg = JSON.stringify({
    type: 'subfolders',
    subfolders: subfolders,
  })
  broadcast(msg)
  waw.updateSubfolders(subfolders)
}
