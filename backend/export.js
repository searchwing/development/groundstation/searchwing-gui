const util = require('util')
const fs = require('fs')
const path = require('path')

const readdir = util.promisify(fs.readdir)
const readFile = util.promisify(fs.readFile)
const copyFile = util.promisify(fs.copyFile)
const mkdir = util.promisify(fs.mkdir)
const exists = util.promisify(fs.exists)

class Exporter {
  constructor (config, folder) {
    this.config = config
    this.folder = folder
  }

  async exportFlagged () {
    console.log('=======')
    console.log(`Starting export of ${this.folder}`)
    for (let side of ['left', 'right']) {
    try{
        const subfolderName = this.config.left_right_folder_names[side]
        const full_path = path.join(
        this.config.datadir,
        this.folder,
        subfolderName
      )
      let contents = await readdir(full_path)
      contents = contents.filter(
        file =>
          !file.startsWith('.') &&
          file !== '0_latest.jpg' &&
          ['.jpg'].some(ending => file.endsWith(ending))
      )
      contents = contents.sort((a, b) => a.localeCompare(b))
      const export_folder = path.join(
        this.config.datadir,
        this.folder,
        'export_flagged',
        subfolderName
      )
      await mkdir(export_folder, { recursive: true })

      for (let file of contents) {
        const jsonPath = `${file}.json`
        try {
          const data = JSON.parse(
            await readFile(path.join(full_path, jsonPath))
          )
          const flagged = data.manual.flagged
          if (flagged) {
            const input_filename_image = path.join(full_path, file)
            const input_filename_json = path.join(full_path, jsonPath)
            const output_filename_image = path.join(export_folder, file)
            const output_filename_json = path.join(export_folder, jsonPath)
            try {
              await copyFile(
                input_filename_image,
                output_filename_image,
                fs.constants.COPYFILE_EXCL
              )
              await copyFile(
                input_filename_json,
                output_filename_json,
                fs.constants.COPYFILE_EXCL
              )
              console.log(`Exported ${subfolderName}/${file}`)
            } catch (err) {
              if (err.code === 'EEXIST') {
                console.log(`File ${subfolderName}/${file} already exists`)
              } else {
                console.error(err)
              }
            }
          }
        } catch (err) {
          // console.error(`Error exporting ${jsonPath}`)
          // console.error(err)
        }
      }
      } catch (err) {
        console.error(err)
      }
    }
    console.log(`Export finished`)
    console.log('=======')
  }
}

module.exports = { Exporter }
