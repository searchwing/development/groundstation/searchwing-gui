const { LeftRightFolderWatcher } = require('./folderWatcher.js')
const { MetadataManager } = require('./metadata.js')

class WhoAndWhat {
  constructor (config) {
    this.config = config
    this.datadir = config.datadir
    this.subfolders = {}
    setInterval(() => this.deleteUnwatchedSubfolders(), 10 * 60 * 1000)
  }

  updateSubfolders (subfolders) {
    subfolders.forEach(subfolder => {
      if (!this.subfolders.hasOwnProperty(subfolder)) {
        // new folder
        this.subfolders[subfolder] = {
          sockets: new Set(),
          watcher: new LeftRightFolderWatcher(
            `${this.datadir}/${subfolder}`,
            this.config
          ),
          metadataManager: new MetadataManager(
            this.datadir,
            subfolder,
            this.config
          )
        }
        // this.subfolders[subfolder].watcher.start()  // Debug
        this.subfolders[subfolder].watcher.onChangedFilesLeft = files =>
          this.onSubfolderFilesUpdate(subfolder, 'left', files)
        this.subfolders[subfolder].watcher.onChangedFilesRight = files =>
          this.onSubfolderFilesUpdate(subfolder, 'right', files)
        this.subfolders[subfolder].watcher.folderIsBusyLeft = () =>
          this.subfolders[subfolder].metadataManager.metadataManagers.left
            .isBusy
        this.subfolders[subfolder].watcher.folderIsBusyRight = () =>
          this.subfolders[subfolder].metadataManager.metadataManagers.right
            .isBusy
      }
    })
    Object.entries(this.subfolders).forEach(([subfolder, struct]) => {
      if (subfolders.indexOf(subfolder) === -1) {
        this.subfolders[subfolder].watcher.stop()
        delete this.subfolders[subfolder]
      }
    })
  }
  async onSubfolderFilesUpdate (subfolder, side, files) {
    if (this.subfolders.hasOwnProperty(subfolder)) {
      const metadataManager = this.subfolders[subfolder].metadataManager
      const [
        newFilePrefixes,
        newAlgoPrefixes,
        removedFilePrefixes
      ] = await metadataManager.onSubfolderFilesUpdate(side, files)
      if (
        newFilePrefixes.length > 0 ||
        removedFilePrefixes.length > 0 ||
        newAlgoPrefixes.length > 0
      ) {
        console.log(
          `${subfolder}/${side} ${newFilePrefixes.length}+ ${
            newAlgoPrefixes.length
          }~ ${removedFilePrefixes.length}-`
        )
        this.sendFilesChanged(
          subfolder,
          side,
          newFilePrefixes,
          newAlgoPrefixes,
          removedFilePrefixes
        )
      }
    }
  }

  sendFilesChanged (
    subfolder,
    side,
    newFilePrefixes,
    newAlgoPrefixes,
    removedFilePrefixes
  ) {
    if (this.subfolders.hasOwnProperty(subfolder)) {
      const metadataManager = this.subfolders[subfolder].metadataManager
      const sockets = this.subfolders[subfolder].sockets
      const newFileData = metadataManager.getDataByPrefixes(
        side,
        newFilePrefixes
      )
      const newAlgoData = metadataManager.getDataByPrefixes(
        side,
        newAlgoPrefixes
      )
      let msg = JSON.stringify({
        type: 'changed_files',
        subfolder: subfolder,
        side: side,
        newFileData: newFileData,
        newAlgoData: newAlgoData,
        removedFiles: removedFilePrefixes
      })
      sockets.forEach(ws => ws.send(msg))
    }
  }

  sendManualDataChanged (subfolder, side, file, changedData) {
    if (this.subfolders.hasOwnProperty(subfolder)) {
      const sockets = this.subfolders[subfolder].sockets
      let msg = JSON.stringify({
        type: 'manual_data_changed',
        subfolder: subfolder,
        side: side,
        file: file,
        changedData: changedData
      })
      sockets.forEach(ws => ws.send(msg))
    }
  }

  socketWatchesSubfolder (ws, subfolder) {
    if (this.subfolders.hasOwnProperty(subfolder)) {
      console.log(
        `Adding listener on ${subfolder} (now ${this.subfolders[subfolder]
          .sockets.size + 1})`
      )
      this.subfolders[subfolder].sockets.add(ws)
      let msg = JSON.stringify({
        type: 'left_right_all_files',
        left: this.subfolders[subfolder].metadataManager.metadataManagers.left
          .files,
        right: this.subfolders[subfolder].metadataManager.metadataManagers.right
          .files
      })
      ws.send(msg)
      if (this.subfolders[subfolder].sockets.size === 1) {
        this.subfolders[subfolder].watcher.start()
      }
    }
  }

  deleteUnwatchedSubfolders () {
    Object.entries(this.subfolders).forEach(([subfolder, struct]) => {
      if (struct.sockets.size === 0 && !struct.watcher.isReset) {
        console.log(
          `Clearing in-memory data of ${subfolder}, as no client is watching`
        )
        struct.watcher.reset()
        struct.metadataManager.reset()
      }
    })
  }

  unsubscribeSocket (ws) {
    Object.entries(this.subfolders).forEach(([subfolder, struct]) => {
      if (struct.sockets.has(ws)) {
        console.log(
          `Removing listener from ${subfolder} (now ${struct.sockets.size - 1})`
        )
        struct.sockets.delete(ws)
        if (struct.sockets.size === 0) {
          struct.watcher.stop()
          // Reset watchers and metadataManagers after 10 minutes, if no client is watching (clear memory)
        }
      }
    })
  }

  socketDisconnected (ws) {
    this.unsubscribeSocket(ws)
  }

  broadcastToSubfolder (subfolder, msg) {
    if (this.subfolders.hasOwnProperty(subfolder)) {
      this.subfolders[subfolder].forEach(ws => ws.send(msg))
    }
  }

  updateMetadata (msg) {
    if (msg.subfolder && this.subfolders.hasOwnProperty(msg.subfolder)) {
      const changedData = this.subfolders[
        msg.subfolder
      ].metadataManager.updateMetadata(msg)
      if (changedData !== false) {
        this.sendManualDataChanged(
          msg.subfolder,
          msg.side,
          msg.file,
          changedData
        )
      }
    }
  }
}

module.exports = { WhoAndWhat }
