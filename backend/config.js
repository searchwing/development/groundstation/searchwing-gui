module.exports = {
  // datadir: '/absolute/path/to/data/dir', // Please only specifiy absolute paths. Comment this line to use backend/data/
  left_right_folder_names: {
    left: 'cam-l',
    right: 'cam-r'
  },
  rootfolder_contents_update_interval: 5,
  subfolder_contents_update_interval: 5,
  frontendSettings: {
    boundingBoxBehavior: 'hideWithShift', // Values: 'showWithShift' or 'hideWithShift'
    showCompassOverlay: true, // Values: true or false
    compassOverlayOpacity: 0.7, // Values: between 0 and 1
    smartCycleMaxTimeDiff: 10.0, // Values: non-negativ numbers
    sidebar_filter_function: 'no_filter_func', // Values: 'no_filter_func', 'view_filter_func', 'flagged_filter_func'
    sidebar_sort_function: 'datetime_sort_func', // Values: 'datetime_sort_func', 'viewed_sort_func', 'filename_sort_func', 'algo_sort_func'
    skip_seconds: 0, // Values: non-negativ numbers. 0 means no filtering
    cycle_mode: 'stay_on_side', // Values: 'stay_on_side', 'alternate_sides', 'smart_cycle', 'filebrowser'
    visitedAreasOnMapColor: [235, 170, 19],
    allImageAreasOnMapColor: [33, 199, 0],
    currentImageAreaOnMapColor: [255,0,0],
    nextImageAreaOnMapColor: [0,255,0],
    filteredImageAreasOnMapColor: [100, 128, 0],
    showVisitedImagesOnMap: true,
    showAllImageAreasOnMap: true,
    showCurrentImageAreaOnMap: true,
    showNextImageAreaOnMap: false,
    showFilteredImageAreasOnMap: false,
    tracksBoxSizeMode: 'positionAccuracy', // Values: 'positionAccuracy','trackBoxSize'
    tracksVisualizationSigma: 2, // sigmas for visualizing track distribution
    result_colors: {
      // Colors of the bounding box overlays and the probability indicators in the sidebar. High=high probability of a boat
      // Probabilities between 0 and 0.5 are shown with an interpolated color between low and medium. Between 0.5 and 1 medium and high are interpolated
      // Colors are defined in RGB between 0 and 255
      high: [255, 0, 0],
      medium: [255, 140, 0],
      low: [0, 255, 0]
    },
    algos: {
      // For every algorithm you can enter a threshold. Bounding boxes below that threshold will be discarded. This threshold is applied
      // before any probabilities are normalized. To show all bounding boxes an algo outputs, enter zero.
      // For every algorithm you can enter a normalization_threshold that you observed to be a sensible decision threshold (below is no boat, above is a boat)
      // The algorithm output probabilities are remapped so that the specified normalization_threshold maps to 50% displayed probability. The remapped
      // probabilities are used to color the sidebar indicators and the bounding box overlays (low probability=green, high probability=red)
      // Set normalization_threshold to -1 or 0.5 to not remap values (displayed probabilities=algo output values)
      detection: {
        frcnn: {
          threshold: 0.0,
          normalization_threshold: 0.05
        }
      },
      classification: {
        efn: {
          threshold: 0.0,
          normalization_threshold: 1e-5
        }
      },
      tracking: {
        CPLinearKalmanMOT: {
          threshold: 0.0,
          normalization_threshold: -1.0
        }
      }
    },
    showSeparateAlgoIndicators: true
  }
}
