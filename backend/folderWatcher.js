const chokidar = require('chokidar')
const util = require('util')
const fs = require('fs')
const { hashElement } = require('folder-hash');

const readdir = util.promisify(fs.readdir)
const lstat = util.promisify(fs.lstat)

// https://stackoverflow.com/questions/33355528/filtering-an-array-with-a-function-that-returns-a-promise
async function async_filter (arr, callback) {
  const fail = Symbol()
  return (await Promise.all(
    arr.map(async item => ((await callback(item)) ? item : fail))
  )).filter(i => i !== fail)
}

async function getImagesCount(folder) {
  try {
    let files = await readdir(folder)
    files = files.filter(
      file =>
        !file.startsWith('.') &&
        file !== '0_latest.jpg' &&
        ['.jpg'].some(ending => file.endsWith(ending))
    )
    return files.length
  } catch (error) {
    if (error.code != 'ENOENT'){ // discard folder not existant messages 
        console.error(`Error reading folder ${folder}: ${error}`)
    }
    return 0
  }
}

async function getFolderText(rootfolder,folders, config) {
  const foldersText = []
  for (const subfolder of folders) {
    const countL = await getImagesCount(`${rootfolder}/${subfolder}/${config.left_right_folder_names['left']}`)
    const countR = await getImagesCount(`${rootfolder}/${subfolder}/${config.left_right_folder_names['right']}`)
    foldersText.push(`${subfolder} (L ${countL} R ${countR})`)
  }
  return foldersText
}

async function checkFolder (
  folder,
  oldContents,
  oldHash,
  onlyFolders = false,
  filterFiles = false,
  config
) {
  let contents = null
  try {
    contents = await readdir(folder)
  } catch (err) {
    contents = []
  }

  try {
    // let isDir = (await lstat(folder + 'abc')).isDirectory()
    // console.log(isDir)
    if (onlyFolders) {
      contentsDir = await async_filter(contents, async subfolder =>
        (await lstat(`${folder}/${subfolder}`)).isDirectory()
      )
      contentsSymDir = await async_filter(contents, async subfolder =>
        (await lstat(`${folder}/${subfolder}`)).isSymbolicLink()
      )
      contents = contentsDir.concat(contentsSymDir)
      contentsText =  await getFolderText(folder,contents, config)
    }
    if (filterFiles !== false) {
      contents = contents.filter(
        file =>
          !file.startsWith('.') &&
          file !== '0_latest.jpg' &&
          filterFiles.some(ending => file.endsWith(ending))
      )
    }
    contents = contents.sort((a, b) => a.localeCompare(b))

    folderHash="" // use hash of folder to detect changes for detection & tracking updates
    if (!onlyFolders) {
      const hashOptions = {
        algo: 'md5',
        encoding: 'binary', // 'base64', 'hex' or 'binary'
        folders: {  },
        files: { include: ['*.json']},
        symbolicLinks: {include: false},
      };
      let folderHashResult = await hashElement(folder, hashOptions);
      folderHash=folderHashResult.hash
    } 
      
    if (
      contents.length !== oldContents.length ||
      JSON.stringify(contents) !== JSON.stringify(oldContents) || // Could be done faster
      (!onlyFolders && folderHash != oldHash)
    ) {
      return {
        'contents': contents,
        'hash': folderHash,
        'contentsText': contentsText
      }
    }
  } catch (err) {
    console.error(err)
  }
  return null
}

class FolderWatcher {
  constructor (config, folder) {
    this.config = config
    this.folder = folder
    this.files = []
    this.files_hash = null
    this.onChangedFiles = () => {}
    this.folderIsBusy = () => false
    // console.log('New FolderWatcher', this.folder)
  }
  start () {
    this.timer = setInterval(
      () => this.checkFolder(),
      this.config.subfolder_contents_update_interval * 1000.0
    )
    this.checkFolder()
  }
  stop () {
    clearInterval(this.timer)
  }
  reset () {
    this.files = []
  }
  async checkFolder () {
    if (!this.folderIsBusy()) {
      const checkResult = await checkFolder(
        this.folder, 
        this.files, 
        this.files_hash, 
        false, 
        ['.jpg','.json'],
         this.config)
      // null if nothing has changed
      if (checkResult) {
        this.files = checkResult['contents']
        this.files_hash = checkResult['hash']
        this.onChangedFiles(this.files)
      }
    } else {
      console.warn(
        `Folder ${
          this.folder
        } is still being processed, not checking for changed contents`
      )
    }
  }
}

class LeftRightFolderWatcher {
  constructor (folder, config) {
    this.folder = folder
    this.config = config
    // console.log('New LRFolderWatcher', this.folder)
    this.onChangedFilesLeft = () => {}
    this.onChangedFilesRight = () => {}
    this.folderIsBusyLeft = () => false
    this.folderIsBusyRight = () => false
    this.isReset = true

    this.folderWatcherLeft = new FolderWatcher(
      this.config,
      `${this.folder}/${this.config.left_right_folder_names.left}`
    )
    this.folderWatcherRight = new FolderWatcher(
      this.config,
      `${this.folder}/${this.config.left_right_folder_names.right}`
    )
    this.folderWatcherLeft.onChangedFiles = files =>
      this.onChangedFilesLeft(files)
    this.folderWatcherRight.onChangedFiles = files =>
      this.onChangedFilesRight(files)
    this.folderWatcherLeft.folderIsBusy = () => this.folderIsBusyLeft()
    this.folderWatcherRight.folderIsBusy = () => this.folderIsBusyRight()
  }
  start () {
    this.folderWatcherLeft.start()
    this.folderWatcherRight.start()
    this.isReset = false
  }
  stop () {
    this.folderWatcherLeft.stop()
    this.folderWatcherRight.stop()
  }
  reset () {
    this.folderWatcherLeft.reset()
    this.folderWatcherRight.reset()
    this.isReset = true
  }
}

class RootFolderWatcher {
  constructor (config, folder) {
    this.config = config
    this.folder = folder
    this.subfolders = []
    this.onChangedSubfolders = () => {}
    console.log('New RootFolderWatcher', this.folder)

    this.timer = setInterval(
      () => this.checkFolder(),
      this.config.rootfolder_contents_update_interval * 1000.0
    )
    this.checkFolder()
  }

  getSubfoldersHomeScreen () {
    let folderdata = [];
    for (let i = 0; i < this.subfolders.length; i++) {
      let folder={}
      folder['subfolder']=this.subfolders[i]
      folder['subfolderText']=this.subfoldersText[i]
      folderdata.push(folder)
    }
    return folderdata
  }

  async checkFolder () {
    const checkResult = await checkFolder(
      this.folder,
      this.subfolders,
      "",
      true,
      false,
      this.config
    )
    // null if nothing has changed
    if (checkResult) {
      this.subfolders = checkResult['contents']
      this.subfoldersText = checkResult['contentsText']
      this.onChangedSubfolders(this.subfolders)
    }
  }
}

module.exports = { FolderWatcher, RootFolderWatcher, LeftRightFolderWatcher }
