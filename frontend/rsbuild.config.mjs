import { defineConfig } from '@rsbuild/core'
import { pluginReact } from '@rsbuild/plugin-react'

export default defineConfig({
  plugins: [
    pluginReact(),
  ],
  html: {
    template: './public/index.html',
  },
  output: {
    distPath: {
      root: 'build',
    },
  },
  server: {
    host: 'localhost',
    proxy: {
      '/ws': {
        target: 'http://localhost:3001',
        ws: true,
      },
      '/image': 'http://localhost:3002',
      '/export': 'http://localhost:3002',
    },
  },
})