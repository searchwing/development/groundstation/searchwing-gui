# Development server

Prepare with

```
npm install
```

Adapt `src/setupProxy.js` to point to the backend (usually `localhost:3001`)

Start development server with

```
npm start
```

# Production build

- tbd
