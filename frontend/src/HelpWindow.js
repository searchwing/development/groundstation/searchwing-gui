import React from 'react'
import { connect } from 'react-redux'
import './HelpWindow.css'

function Key (props) {
  return <code className='Key'>{props.children}</code>
}

class HelpWindow extends React.Component {
  render () {
    return (
      <main className='HelpWindow'>
        <header>
          <b>Help</b>
        </header>

        <header>Cycle Modes</header>

        <div className='TextRow'>
          There are different modes you can select to cycle through images. They
          mainly define what happens when you press next or previous, so in
          which order you see the images. Most modes are focused on showing
          images smartly in their chronological order, so if you choose a sort
          function that is not <i>datetime</i>, you will only be able to use{' '}
          <i>stay on side</i> as cycle mode:
        </div>

        <div className='SectionContainer'>
          <section>
            <header>Stay on side:</header>
            <main>
              The next image will be from the same camera as the image before.
              You just go up or down in the list of images that is currently
              selected.
              <div className='keyArea'>
                <b>Keys</b>:{' '}
                <section>
                  <Key>Right</Key>/<Key>Down</Key>/<Key>D</Key>/<Key>S</Key>:
                  Next image,
                </section>
                <section>
                  <Key>Left</Key>/<Key>Up</Key>/<Key>A</Key>/<Key>W</Key>:
                  Previous Image
                </section>
              </div>
            </main>
          </section>
          <section>
            <header>Filebrowser:</header>
            <main>
              This mode is very similar to <i>stay on side</i>. The keys are
              assigned a little differently:
              <div className='keyArea'>
                <b>Keys</b>:{' '}
                <section>
                  <Key>Up</Key>/<Key>W</Key>: Previous image in current list,
                </section>
                <section>
                  <Key>Down</Key>/<Key>S</Key>: Next image in current list,
                </section>
                <section>
                  <Key>Right</Key>/<Key>D</Key>: Go to chronologically closest
                  image on the right side
                </section>
                <section>
                  <Key>Left</Key>/<Key>A</Key>: Go to chronologically closest
                  image on the left side
                </section>
              </div>
            </main>
          </section>
          <section>
            <header>Alternate Sides:</header>
            <main>
              In this mode, you go through all images in their{' '}
              <b>chronological order</b>. If the two cameras take pictures with
              the same frequency, you will normally see a left image, then a
              right image, then left again. As the two cameras are not
              synchronized, it will sometimes happen that you see two images in
              a row from the same camera, if this is according to the
              chronological order. In this mode you will not miss any images
              (given that they are downloaded in chronological order).
              <div className='keyArea'>
                <b>Keys</b>:{' '}
                <section>
                  <Key>Right</Key>/<Key>Down</Key>/<Key>D</Key>/<Key>S</Key>:
                  Next image,
                </section>
                <section>
                  <Key>Left</Key>/<Key>Up</Key>/<Key>A</Key>/<Key>W</Key>:
                  Previous Image
                </section>
              </div>
            </main>
          </section>
          <section>
            <header>Smart Cycle:</header>
            <main>
              The smart cycle mode lets you view images from one camera for a
              set period of time (to combine chronological and spatial
              consistancy for that period), before switching to the other
              camera. If you select a file in the sidebar, an anchors is placed
              in each list: at the selected file and at the closest file of the
              other side (indicated by thin dotted lines). As you proceed
              looking through the images on one side, you will jump to the other
              side's anchor as soon as the anchor time difference exceeds a
              threshold (default: 10 seconds).
              <br />
              <b>Warning</b>: Although all images should be visited by this
              method, you should be careful to monitor the sidebar for any
              skipped images. <b>Known issue</b>: Whenever you select an image
              manually, the anchors are reset to the clicked image, and the
              nearest image on the other side. Images between the old anchors
              (before clicking) might have been skipped by this action.{' '}
              <b>Known issue</b>: Even if you start with the chronologically
              first image, the first image of the other side is skipped (be sure
              to check that image manually).
              <div className='keyArea'>
                <b>Keys</b>:{' '}
                <section>
                  <Key>Right</Key>/<Key>Down</Key>/<Key>D</Key>/<Key>S</Key>:
                  Next image,
                </section>
                <section>
                  <Key>Left</Key>/<Key>Up</Key>/<Key>A</Key>/<Key>W</Key>:
                  Previous Image
                </section>
              </div>
            </main>
          </section>
        </div>

        <header>General Keymap:</header>
        <div className='SectionContainer'>
          <section>
            <header>
              <Key>Enter</Key>:
            </header>
            <main>Mark current image as viewed and go to next image</main>
          </section>
          <section>
            <header>
              <Key>Space</Key>:
            </header>
            <main>Flag current image as interesting</main>
          </section>
          <section>
            <header>
              <Key>Shift</Key>:
            </header>
            <main>
              Some algorithms compute bounding boxes around regions that could
              contain boats. They can either be overlayed over the image by
              default, and hidden with the <Key>shift</Key> key (hold the key
              down), or only overlayed while <Key>shift</Key> is pressed. The
              behavior can be changed in the settings.
            </main>
          </section>
          <section>
            <header>
              <Key>5</Key>:
            </header>
            <main>
                Reset brightness and contrast settings.
            </main>
          </section>
          <section>
            <header>
              <Key>6</Key>:
            </header>
            <main>
                Decrease image contrast.
            </main>
          </section>
          <section>
            <header>
              <Key>7</Key>:
            </header>
            <main>
                Increase image contrast.
            </main>
          </section>
          <section>
            <header>
              <Key>8</Key>:
            </header>
            <main>
                Decrease image brightness.
            </main>
          </section>
          <section>
            <header>
              <Key>9</Key>:
            </header>
            <main>
                Increase image brightness.
            </main>
          </section>
          <section>
            <header>
              <Key>m</Key>:
            </header>
            <main>
              Toggle measurement tool for with <Key>m</Key>. It allows to measure
              the distance between two selected points. Note that due to technical reasons, the accuracy is quite low at the moment. It only should provide you a rough estimate.
            </main>
          </section>
          <section>
            <header>
              <Key>p</Key>:
            </header>
            <main>
              Toggle position tool for with <Key>p</Key>. It allows to visualize the position of the clicked point in Lat/Lon & Degrees Minutes Seconds format.
            </main>
          </section>
        </div>

        <header>Object Detection & Tracking</header>

        <div className='TextRow'>
          <p>
            The <i>detection</i> algorithm  produce bounding boxes in the
            images, each with a probability of containing a boat. The{' '}
            <b>maximum</b> bounding box probability of an image for each
            algorithm is shown as a small colored square in the sidebar. Each
            bounding box is drawn on the image in the image viewer in that same
            color. 
          </p>
          <p>
            After the detection a <i>tracking</i> algorithm is used to combine all 
            detections of multiple images. One real object is tracked over all Images 
            containing it. After the tracking is finished, the tracker outputs a
            <b>tracking ID</b>, the <b>trackingstate</b>, its <b>state/position accuracy</b>  
            and the <b>amount of detections</b> which where used in the tracking of that real object.
            The gaussian distribution of the state/position is drawn as a circle and
            shows two sigma of the standard deviation of the position gaussian. The
            tracker uses a constant position model for its state transition, therefore
            currently its not possible to determine the speed and heading of the object.
          </p>
          <p>
            The following section explains the other output variables of the tracker:
          </p>
          <div className='SectionContainer'>
            <section>
              <header>Tracking ID:</header>
              <main>
                A unique number which identifies the track.
              </main>
            </section>
            <section>
              <header>Trackingstate:</header>
              <main>
                <div className='SectionContainer'>
                  <section>
                    <header>Predicted:</header>
                    <main>
                      For this track no detection was found in the current image. The position of this track is therefore predicted according to prior knowledge/detection.
                    </main>
                  </section>
                  <section>
                    <header>Measured:</header>
                    <main>
                      For this track a detection was found in the current image. The position / state of the track was updated based on this new detection.
                    </main>
                  </section>
                  <section>
                    <header>Validated:</header>
                    <main>
                      This track was detected more than 5 times, therefore this must be for sure a real boat or object in the water, and not just a wave or reflection.
                      Those in contrast occure at the same global position only for a limited amount of time.
                    </main>
                  </section>
                </div>
              </main>
            </section>
            <section>
              <header>Amount of detections:</header>
              <main>
                In this number of images, the particular track was recognized.
              </main>
            </section>
          </div>
          <p>
             For example if you see a track in the image with <b>75V / 8</b> written next to it, this means that this track got the ID 75, is validated and it was in all images 8 times detected.
          </p>
        </div>

        <header>Settings</header>

        <div className='TextRow'>
          <p>
            The default settings are read from
            'searchwing-gui/backend/config.js' when the backend is started. You
            can change some settings (e.g. the sort function) above the sidebar,
            others can be changed in the settings window (e.g. <Key>shift</Key>{' '}
            behavior). These settings are reset to defaults whenever you reload
            the UI in your browser. To change them permanently, change their
            default values in the 'config.js' file.{' '}
            <b>After a change, the backend has to be restarted.</b> Please only
            set ordinal values to the proposed values in the associated comment,
            and numbers only to resonable values (carefully read the comments!).
          </p>
          <p>
            There are also settings for the backend in the 'config.js' file.
            Consider changing <i>subfolder_contents_update_interval</i> to a
            high value, if the UI gets too slow. This changes how often the
            backend looks for new files in the data folder. Whenever there are
            new files, the UI has to update, which can slow down the browser a
            lot. By increasing this value e.g. to 30 seconds, you will only see
            new files every 30 seconds, but no big updates to the UI have to be
            done during that time.
          </p>
          <p>
            Before pulling the repository searchwing-gui, consider resetting
            your config.js to its defaults, to avoid merge conflicts:
            <br />
            'git checkout -- backend/config.js'
          </p>
        </div>

        <header>Additional information</header>

        <div className='SectionContainer'>
          <section>
            <header>Browser:</header>
            <main>
              We have found that the UI is much faster in <b>Google Chrome</b>{' '}
              than e.g. in Firefox
            </main>
          </section>
          <section>
            <header>Map:</header>
            <main>
              On the bottom right you can see a map of the current flight. The
              yellow dot denotes the position of the plane on the flight path.
              The red line is the direction that the plane is pointing, while
              the grey line shows the direction of the selected camera (the
              camera of the selected image).
            </main>
          </section>
          <section>
            <header>Metadata Inspector:</header>
            <main>
              Shows additional information about the selected image such as all
              exif information and algorithmic classification and detection
              results.
            </main>
          </section>
        </div>
      </main>
    )
  }
}

const mapDispatchToProps = {}
const mapStateToProps = state => ({})

export default connect(mapStateToProps, mapDispatchToProps)(HelpWindow)
