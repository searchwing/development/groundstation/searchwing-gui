import React from 'react'
import { connect } from 'react-redux'
import TreeView from 'react-treeview'
import 'react-treeview/react-treeview.css'
import './ExifInspector.css'

function displayVal (val) {
  if (typeof val === 'boolean') {
    return val ? 'true' : 'false'
  }
  return val
}

function displayArray (arr) {
  try {
    return JSON.stringify(arr)
  } catch (err) {
    return '?'
  }
}

function nodeToTreeView (node, name) {
  if (typeof node === 'object') {
    if (Array.isArray(node)) {
      return (
        <div key={name}>
          {name}: {displayArray(node)}
        </div>
      )
    } else {
      const children = Object.entries(node).map(([name, node]) =>
        nodeToTreeView(node, name)
      )
      return (
        <TreeView key={name} defaultCollapsed={true} nodeLabel={name}>
          {children}
        </TreeView>
      )
    }
  }
  return (
    <div key={name}>
      {name}: {displayVal(node)}
    </div>
  )
}

class ExifInspector extends React.Component {
  shouldComponentUpdate (nextProps, nextState) {
    if (nextProps.images.selected_file !== this.props.images.selected_file) {
      return true
    }
    return false
  }

  render () {
    try {
      const curr_img = this.props.images.selected_file
      const metadata = this.props.images.tree[curr_img.side][curr_img.file]
      // const exif = JSON.stringify(metadata.exif, null, 4)
      return (
        <div className='ExifInspector'>
          {nodeToTreeView(metadata.exif, 'Exif Data')}
          {nodeToTreeView(metadata.manual, 'Manual Annotations')}
          {nodeToTreeView(metadata.algoResults, 'Algorithmic Results')}
        </div>
      )
    } catch (err) {
      return <div className='ExifInspector' />
    }
  }
}

const mapDispatchToProps = {}
const mapStateToProps = state => ({
  images: state.images
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExifInspector)
