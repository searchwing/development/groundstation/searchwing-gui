import React from 'react'
import { connect } from 'react-redux'
import {
  selectSubfolder,
  subscribeToImages,
  unsubscribeFromImages
} from './reducers/imagesReducer.js'
import ImageViewer from './ImageViewer.js'
import Sidebar from './Sidebar.js'
import PopupWindow from './PopupWindow.js'
import HelpWindow from './HelpWindow.js'
import SettingsWindow from './SettingsWindow'
import './FolderWatch.css'
import { ConvertDDToDMSString } from './Tools.js'

function degrees_to_float (ds) {
  return ds[0] + ds[1] / 60 + ds[2] / 3600
}

class FolderWatch extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      helpWindowOpen: false,
      settingsWindowOpen: false
    }
  }
  openHelpWindow = () => {
    this.setState({ ...this.state, helpWindowOpen: true })
  }
  closeHelpWindow = () => {
    this.setState({ ...this.state, helpWindowOpen: false })
  }
  openSettingsWindow = () => {
    this.setState({ ...this.state, settingsWindowOpen: true })
  }
  exportFlagged = async () => {
    await fetch(`/export/flagged/${this.props.subfolder}`, { method: 'POST' })
  }
  closeSettingsWindow = () => {
    this.setState({ ...this.state, settingsWindowOpen: false })
  }
  componentDidMount () {
    this.props.subscribeToImages(this.props.subfolder)
  }
  componentWillUnmount () {
    this.props.unsubscribeFromImages()
  }
  shouldComponentUpdate (nextProps, nextState) {
    if (this.props.date !== nextProps.date) {
      return true
    } else if (this.props.tree !== nextProps.tree) {
      return false
    }
    return true
  }

  render () {
    if (this.props.connected === 'not_yet_connected') {
      return <div>Connecting to backend...</div>
    } else if (this.props.connected === 'lost_connection') {
      return <div>Not connected to backend. Trying to reconnect...</div>
    }
    let selectedFile = null
    try {
      selectedFile = this.props.tree[this.props.selected_file.side][
        this.props.selected_file.file
      ]
    } catch (err) {}
    let timeStr = null
    try {
      const date = new Date(selectedFile.exif.exif.DateTimeOriginal)
      timeStr =
        date.toLocaleTimeString() + '.' + selectedFile.exif.exif.SubSecTimeOriginal
    } catch (err) {}
    return (
      <div className='FolderWatch'>
        {this.state.helpWindowOpen && (
          <PopupWindow onClose={this.closeHelpWindow}>
            <HelpWindow />
          </PopupWindow>
        )}
        {this.state.settingsWindowOpen && (
          <PopupWindow onClose={this.closeSettingsWindow}>
            <SettingsWindow />
          </PopupWindow>
        )}
        <header>
          <button onClick={() => this.props.selectSubfolder(null)}>
            &lt; back
          </button>
          <div className='spacer' />
          <main>
            <section>
              Folder: <b>{this.props.subfolder}</b>
            </section>
            {this.props.date && (
              <React.Fragment>
                <div className='spacer' />
                <section>
                  Date: <b>{this.props.date}</b>
                </section>
              </React.Fragment>
            )}
            {timeStr !== null && (
              <React.Fragment>
                <div className='spacer' />
                <section>
                  Time: <b>{timeStr}</b>
                </section>
              </React.Fragment>
            )}
            {selectedFile &&
              selectedFile.exif &&
              selectedFile.exif.gps &&
              selectedFile.exif.gps.simplifiedLatLon && (
                <React.Fragment>
                  <div className='spacer' />
                  <section>
                    GPS:{' '}
                    <b>
                      {ConvertDDToDMSString(degrees_to_float(selectedFile.exif.gps.GPSLatitude),false)}{' '}
                      {ConvertDDToDMSString(degrees_to_float(selectedFile.exif.gps.GPSLongitude),true)}{' '}
                      {selectedFile.exif.gps.simplifiedLatLon.alt}
                    </b>
                  </section>
                </React.Fragment>
              )}
            {this.props.selected_file && (
              <React.Fragment>
                <div className='spacer' />
                <section>
                  File:{' '}
                  <b>
                    [{this.props.selected_file.side}]{' '}
                    {this.props.selected_file.file}
                  </b>
                </section>
              </React.Fragment>
            )}
          </main>

          <div className='spacer' />
          <button onClick={this.openHelpWindow}>Help</button>
          <div className='mini-spacer' />
          <button onClick={this.openSettingsWindow}>Settings</button>
          <div className='mini-spacer' />
          <button onClick={this.exportFlagged}>Export Flagged</button>
        </header>
        <main>
          <section>
            {this.props.selected_file && (
              <ImageViewer
                side={this.props.selected_file.side}
                file={this.props.selected_file.file}
              />
            )}
          </section>
          <aside>
            <Sidebar />
          </aside>
        </main>
      </div>
    )
  }
}

const mapDispatchToProps = {
  selectSubfolder,
  subscribeToImages,
  unsubscribeFromImages
}
const mapStateToProps = state => ({
  connected: state.connection.connected,
  subfolder: state.images.subfolder,
  date: state.images.date,
  selected_file: state.images.selected_file,
  tree: state.images.tree
})

export default connect(mapStateToProps, mapDispatchToProps)(FolderWatch)
