import {
    lusolve, multiply
  } from 'mathjs'

export function calculateHomographicTransform(source, destination){
    // Inspired by https://towardsdatascience.com/understanding-homography-a-k-a-perspective-transformation-cacaed5ca17
    // Calculates the entries of the Homography matrix between two sets of matching points.
    //Args
    //----
    //    - `source`: Source points where each point is int (x, y) format.
    //    - `destination`: Destination points where each point is int (x, y) format.
    //Returns
    //----
    //    - A array of shape (3, 3) representing the Homography matrix.

    console.assert(source.length >= 4, "must provide more than 4 source points")
    console.assert(destination.length >= 4, "must provide more than 4 destination points")
    console.assert(source.length === destination.length, "source and destination must be of equal length")
    var A = []
    var b = []
    for (let i = 0; i < source.length; i++){
        let s_x = source[i][0]
        let s_y = source[i][1]
        let d_x = destination[i][0]
        let d_y = destination[i][1]
        A.push([s_x, s_y, 1, 0, 0, 0, (-d_x)*(s_x), (-d_x)*(s_y)])
        A.push([0, 0, 0, s_x, s_y, 1, (-d_y)*(s_x), (-d_y)*(s_y)])
        b.push(d_x)
        b.push(d_y)
    }
    let h = lusolve(A, b)
    h = [
        [h[0][0], h[1][0], h[2][0]],
        [h[3][0], h[4][0], h[5][0]],
        [h[6][0], h[7][0], 1],
    ]
    return h
}


export function projectPoint(homographicTransformMatrix, point){
    const point3 = [...point]
    point3.push(1)
    var transformedPoint = multiply(homographicTransformMatrix, point3)
    return [transformedPoint[0]/ transformedPoint[2], transformedPoint[1]/transformedPoint[2]]
}

export function calculateDistanceProjectedPoints(homographicTransformMatrix, point1, point2){
    // Taken from https://www.movable-type.co.uk/scripts/latlong.html
    const point1Transformed = projectPoint(homographicTransformMatrix, point1)
    const point2Transformed = projectPoint(homographicTransformMatrix, point2)
    
    const lat1 = point1Transformed[0]
    const lon1 = point1Transformed[1]
    const lat2 = point2Transformed[0]
    const lon2 = point2Transformed[1]
    const R = 6371e3; // metres
    const φ1 = lat1 * Math.PI/180; // φ, λ in radians
    const φ2 = lat2 * Math.PI/180;
    const Δφ = (lat2-lat1) * Math.PI/180;
    const Δλ = (lon2-lon1) * Math.PI/180;

    const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    const d = R * c; // in metres
    return d
}