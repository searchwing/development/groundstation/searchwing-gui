import React from 'react'
import { connect } from 'react-redux'
import {
  setFileViewed,
  prevImage,
  nextImage,
  goToOtherSide,
  toggleFileFlagged,
  remap_threshold
} from './reducers/imagesReducer.js'
// import colormap from 'colormap'
import lerp from 'lerp'
import { OverlayCompass } from './Compass'
import './ImageViewer.css'
import { calculateHomographicTransform, calculateDistanceProjectedPoints, projectPoint } from './HomographicTransforms.js'
import { ConvertDDToDMSString } from './Tools.js'

var scaleFactor = 1.1
const cacheNameDivider = ':/&'

function interpolateColor (start, end, v) {
  return [
    lerp(start[0], end[0], v),
    lerp(start[1], end[1], v),
    lerp(start[2], end[2], v)
  ]
}

function getColormapColor (prob, result_colors) {
  if (prob <= 0.5) {
    return interpolateColor(result_colors.low, result_colors.medium, prob * 2)
  } else {
    return interpolateColor(
      result_colors.medium,
      result_colors.high,
      (prob - 0.5) * 2
    )
  }
}

// const colormap = generateColormap([255,255,255], [255,0,0])

// const _colormap = colormap({
//   colormap: 'freesurface-red',
//   nshades: 10,
//   format: 'hex',
//   alpha: 1
// })
// function interpolateColormap (prob) {
//   prob = (1.0 - prob) / 2 + 0.5
//   let index = parseInt(prob * _colormap.length)
//   index = Math.max(0, Math.min(index, _colormap.length - 1))
//   return _colormap[index]
// }
export function interpolateColormap (prob, result_colors) {
  if (prob < 0) {
    return 'black'
  }
  const color = getColormapColor(prob, result_colors)
  return `rgb(${color[0]}, ${color[1]}, ${color[2]})`
}

// http://phrogz.net/tmp/canvas_zoom_to_cursor.html
function trackTransforms (ctx) {
  var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
  if (!ctx.originalFunctions) {
    // Since we are calling trackTransforms several times, we want to store the the original functions in order to avoid potential large callback stacks
    // e.g. the scale is called as many times as trackTransforms was called!
    ctx.originalFunctions = {
      scale: ctx.scale,
      save: ctx.save,
      restore: ctx.restore,
      rotate: ctx.rotate,
      translate: ctx.translate,
      transform: ctx.transform,
      setTransform: ctx.setTransform
    }
  }
  var xform = svg.createSVGMatrix()
  ctx.getTransform = function () {
    return xform
  }

  var savedTransforms = []
  ctx.save = function () {
    savedTransforms.push(xform.translate(0, 0))
    return ctx.originalFunctions['save'].call(ctx)
  }
  ctx.restore = function () {
    xform = savedTransforms.pop()
    return ctx.originalFunctions['restore'].call(ctx)
  }

  ctx.scale = function (sx, sy) {
    xform = xform.scaleNonUniform(sx, sy)
    return ctx.originalFunctions['scale'].call(ctx, sx, sy)
  }
  ctx.rotate = function (radians) {
    xform = xform.rotate((radians * 180) / Math.PI)
    return ctx.originalFunctions['rotate'].call(ctx, radians)
  }
  ctx.translate = function (dx, dy) {
    xform = xform.translate(dx, dy)
    return ctx.originalFunctions['translate'].call(ctx, dx, dy)
  }
  ctx.transform = function (a, b, c, d, e, f) {
    var m2 = svg.createSVGMatrix()
    m2.a = a
    m2.b = b
    m2.c = c
    m2.d = d
    m2.e = e
    m2.f = f
    xform = xform.multiply(m2)
    return ctx.originalFunctions['transform'].call(ctx, a, b, c, d, e, f)
  }
  ctx.setTransform = function (a, b, c, d, e, f) {
    xform.a = a
    xform.b = b
    xform.c = c
    xform.d = d
    xform.e = e
    xform.f = f
    return ctx.originalFunctions['setTransform'].call(ctx, a, b, c, d, e, f)
  }
  var pt = svg.createSVGPoint()
  ctx.transformedPoint = function (x, y) {
    pt.x = x
    pt.y = y
    return pt.matrixTransform(xform.inverse())
  }
}

class ImageViewer extends React.Component {
  image_cache = {}
  confidenceMargin = 20

  constructor (props) {
    super(props)
    this.state = {
      showBoundingBoxes:
        props.images.settings.boundingBoxBehavior === 'hideWithShift',
      result_colors: this.props.images.settings.result_colors,
      brightness: 100,
      contrast: 100,
      showMeasurementTool: false,
      showPositionTool: false
    }
    this.measurementTool = [];
    this.positionTool = [];
  }

  setViewed = () => {
    setFileViewed(this.props.images.subfolder, this.props.side, this.props.file)
  }
  toggleFlagged = () => {
    toggleFileFlagged(
      this.props.images.subfolder,
      this.props.side,
      this.props.file
    )
  }
  cleanup_cache = () => {
    Object.keys(this.image_cache).forEach(key => {
      const [side, file] = key.split(cacheNameDivider)
      const isSameFile = fileStruct =>
        fileStruct && fileStruct.side === side && fileStruct.file === file
      if (
        !isSameFile(this.props.images.selected_file) &&
        !isSameFile(this.props.images.next_file) &&
        !isSameFile(this.props.images.prev_file)
      )
        delete this.image_cache[key]
    })
    // console.log('cleanup_cache:', Object.keys(this.image_cache))
  }
  shouldComponentUpdate (nextProps, nextState) {
    return (
      nextProps.images.selected_file !== this.props.images.selected_file ||
      nextProps.images.next_file !== this.props.images.next_file ||
      nextProps.images.prev_file !== this.props.images.prev_file ||
      nextProps.images.settings !== this.props.images.settings ||
      nextState.showBoundingBoxes !== this.state.showBoundingBoxes
    )
  }
  componentDidUpdate (prevProps, prevState) {
    if (
      prevProps.images.selected_file !== this.props.images.selected_file ||
      prevProps.images.next_file !== this.props.images.next_file ||
      prevProps.images.prev_file !== this.props.images.prev_file
    ) {
      this.cleanup_cache()
    }
    if (prevProps.images.selected_file !== this.props.images.selected_file) {
      this.initCanvas()
      this.loadImage(this.props.images.selected_file)
    }
    if (prevProps.images.next_file !== this.props.images.next_file) {
      this.loadImage(this.props.images.next_file)
    }
    if (prevProps.images.prev_file !== this.props.images.prev_file) {
      this.loadImage(this.props.images.prev_file)
    }
    if (prevProps.images.settings !== this.props.images.settings) {
      this.setState({
        showBoundingBoxes:
          this.props.images.settings.boundingBoxBehavior === 'hideWithShift',
        result_colors: this.props.images.settings.result_colors
      })
      this.canvasRedraw()
    }
    if (
      prevProps.images.newAlgoData_for_selected_image_counter !==
      this.props.images.newAlgoData_for_selected_image_counter
    ) {
      this.canvasRedraw()
    }
  }

  onKeyDown = event => {
    if (
      event.code === 'ArrowLeft' ||
      event.code === 'ArrowUp' ||
      event.code === 'ArrowRight' ||
      event.code === 'ArrowDown' ||
      event.code === 'Space'
    ) {
      // Prevent arrow keys from scrolling the sidebar
      event.preventDefault()
      return false
    }
    if (event.key === 'Shift') {
      this.setState({
        showBoundingBoxes:
          this.props.images.settings.boundingBoxBehavior !== 'hideWithShift'
      })
      this.canvasRedraw()
    }
    if (event.key === "5" ) {
      this.setState({
        contrast: 100,
        brightness: 100
      })
      this.canvasRedraw()
    }
    if (event.key === "6" ) {
      this.setState({
        contrast: Math.max(this.state.contrast - 5, 0)
      })
      this.canvasRedraw()
    }
    if (event.key === "7" ) {
      this.setState({
        contrast: this.state.contrast + 5
      })
      this.canvasRedraw()
    }
    if (event.key === "8" ) {
      this.setState({
        brightness: Math.max(this.state.brightness - 5, 0)
      })
      this.canvasRedraw()
    }
    if (event.key === "9" ) {
      this.setState({
        brightness: this.state.brightness + 5
      })
      this.canvasRedraw()
    }
    if (event.key === 'm') {
      this.setState({
        showMeasurementTool: !this.state.showMeasurementTool
      })
      this.canvasRedraw()
    }
    if (event.key === 'p') {
      this.setState({
        showPositionTool: !this.state.showPositionTool
      })
      this.canvasRedraw()
    }
  }
  
  onKeyUp = event => {
    // console.log(event)
    if (event.code === 'ArrowLeft' || event.key === 'a') {
      if (this.props.images.settings.cycle_mode === 'filebrowser') {
        if (this.props.side === 'right') {
          this.props.goToOtherSide()
        }
      } else {
        this.props.prevImage()
      }
    } else if (event.code === 'ArrowUp' || event.key === 'w') {
      this.props.prevImage()
    } else if (event.code === 'ArrowRight' || event.key === 'd') {
      if (this.props.images.settings.cycle_mode === 'filebrowser') {
        if (this.props.side === 'left') {
          this.props.goToOtherSide()
        }
      } else {
        this.props.nextImage()
      }
    } else if (event.code === 'ArrowDown' || event.key === 's') {
      this.props.nextImage()
    } else if (event.key === 'Enter') {
      // event.key === 'Enter' also considers Numpad Enter
      this.setViewed()
      this.props.nextImage()
    } else if (event.code === 'Space') {
      this.toggleFlagged()
    } else if (event.key === 'Shift') {
      this.setState({
        showBoundingBoxes:
          this.props.images.settings.boundingBoxBehavior === 'hideWithShift'
      })
      this.canvasRedraw()
    }
  }

  onMouseDown = event => {
    const canvas = this.canvasRef
    const ctx = this.canvasContext
    document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect =
      'none'
    this.lastX = event.offsetX || event.pageX - canvas.offsetLeft
    this.lastY = event.offsetY || event.pageY - canvas.offsetTop
    this.dragStart = ctx.transformedPoint(this.lastX, this.lastY)
    this.dragged = false
  }

  onMouseMove = event => {
    const canvas = this.canvasRef
    const ctx = this.canvasContext
    this.lastX = event.offsetX || event.pageX - canvas.offsetLeft
    this.lastY = event.offsetY || event.pageY - canvas.offsetTop
    this.dragged = true
    if (this.dragStart) {
      var pt = ctx.transformedPoint(this.lastX, this.lastY)
      ctx.translate(pt.x - this.dragStart.x, pt.y - this.dragStart.y)
      this.canvasRedraw()
    }
  }
  
  onMouseUp = event => {
    this.dragStart = null
    // if (!this.dragged) { // TODO: Change this part, so that it's called only after the second click (zoom on double click)
    //   this.zoom(event.shiftKey ? -1 : 1)
    // }
  }
  onMouseWheel = event => {
    var delta = event.wheelDelta
      ? event.wheelDelta / 40
      : event.detail
      ? -event.detail
      : 0
    if (delta) {
      this.zoom(delta)
    }
    return event.preventDefault() && false
  }

  onMouseClicked = event => {
    if (this.state.showMeasurementTool){
      const canvas = this.canvasRef
      const ctx = this.canvasContext
      var XPoint_m = event.offsetX || event.pageX - canvas.offsetLeft
      var YPoint_m = event.offsetY || event.pageY - canvas.offsetTop
      var point_m = ctx.transformedPoint(XPoint_m, YPoint_m)
      this.updateMeasurementsPoints(point_m.x, point_m.y)
      this.canvasRedraw();
    }
    if (this.state.showPositionTool){
      const canvas = this.canvasRef
      const ctx = this.canvasContext
      var XPoint_p = event.offsetX || event.pageX - canvas.offsetLeft
      var YPoint_p = event.offsetY || event.pageY - canvas.offsetTop
      var point_p = ctx.transformedPoint(XPoint_p, YPoint_p)
      this.updatePositionPoint(point_p.x, point_p.y)
      this.canvasRedraw();
    }
  }

  updateMeasurementsPoints(x, y){
    if (this.measurementTool.length >= 2) {
      this.measurementTool = []
    }
    this.measurementTool.push({
      "x":x,
      "y":y
    })
  }

  updatePositionPoint(x, y){
    if (this.positionTool.length >= 1) {
      this.positionTool = []
    }
    this.positionTool.push({
      "x":x,
      "y":y
    })
  }

  drawMeasurements(){
    if(this.state.showMeasurementTool){
      
      document.body.style.cursor = 'crosshair';
      // ToDo Visualize that the measurement tool is turned on
      const ctx = this.canvasContext
      var StartPoint = ctx.transformedPoint(0, 0)
      var EndPoint = ctx.transformedPoint(0,1)
      var scalingFactor = Math.sqrt((StartPoint.x - EndPoint.x)**2 + (StartPoint.y - EndPoint.y)**2)
      var pointWidth = 10 * scalingFactor
      var textSize = 20 * scalingFactor
      ctx.font = textSize + 'px serif';
      ctx.strokeStyle = 'red';

      this.measurementTool.forEach(point => {
        ctx.fillStyle = "red";
        // Calculate somehow the scaling factor
        ctx.fillRect(point.x - pointWidth/2, point.y - pointWidth/2, pointWidth, pointWidth);
  
      });
      if (this.measurementTool.length === 2){
        if (this.projectionMatrix == null) {
          this.updateHomographicTransform()
        }
        ctx.lineWidth = pointWidth/4;
        ctx.beginPath();
        ctx.moveTo(this.measurementTool[0].x, this.measurementTool[0].y);
        ctx.lineTo(this.measurementTool[1].x, this.measurementTool[1].y);
        ctx.stroke();
        const vectorX= this.measurementTool[1].x - this.measurementTool[0].x
        const vectorY= this.measurementTool[1].y - this.measurementTool[0].y
        var distanceString = ""
        if (this.projectionMatrix){
          const distance = calculateDistanceProjectedPoints(this.projectionMatrix, 
            [this.measurementTool[0].x, this.measurementTool[0].y],
            [this.measurementTool[1].x, this.measurementTool[1].y]
          )
          distanceString = ": ~ " + distance.toFixed(2) + " ± " + (distance * 0.25).toFixed(2) + "[m]"

        } else {
          const distance = Math.sqrt(vectorX**2 + vectorY**2)
          distanceString = ": " + distance.toFixed(2) + "[Pixel]"
        }
        ctx.fillText("Dist " + distanceString, this.measurementTool[0].x + vectorX/2, this.measurementTool[0].y + vectorY/2)
      }
    } else {
      this.measurementTool = [];
      document.body.style.cursor = 'default';
    }

  }

  drawPosition(){
    if(this.state.showPositionTool){
      
      document.body.style.cursor = 'crosshair';
      // ToDo Visualize that the measurement tool is turned on
      const ctx = this.canvasContext
      var StartPoint = ctx.transformedPoint(0, 0)
      var EndPoint = ctx.transformedPoint(0,1)
      var scalingFactor = Math.sqrt((StartPoint.x - EndPoint.x)**2 + (StartPoint.y - EndPoint.y)**2)
      var pointWidth = 10 * scalingFactor
      var textSize = 20 * scalingFactor
      ctx.font = textSize + 'px serif';
      ctx.fillStyle = "red";

      if (this.positionTool.length === 1){
        if (this.projectionMatrix == null) {
          this.updateHomographicTransform()
        }
        if (this.projectionMatrix){
          //LatLon Format
          const pointTransformed = projectPoint(this.projectionMatrix, [this.positionTool[0].x, this.positionTool[0].y])
          ctx.fillText("Lat " + pointTransformed[0].toFixed(6) + " Lon "+ pointTransformed[1].toFixed(6), this.positionTool[0].x+10*scalingFactor, this.positionTool[0].y-10*scalingFactor)
          
          //DMS Format
          const latDMS = ConvertDDToDMSString(pointTransformed[0],false)
          const lonDMS = ConvertDDToDMSString(pointTransformed[1],true)
          var DMSString = latDMS + " " + lonDMS 
          ctx.fillText(DMSString, this.positionTool[0].x+10*scalingFactor, this.positionTool[0].y+textSize+3*scalingFactor)  

          //Crosshair
          ctx.strokeStyle = 'red';
          ctx.lineWidth = pointWidth/4;
          const x = this.positionTool[0].x;
          const y = this.positionTool[0].y;
          const length=10*scalingFactor;
          ctx.beginPath();
          ctx.moveTo(x - length, y);
          ctx.lineTo(x + length, y);
          ctx.stroke();
          ctx.beginPath();
          ctx.moveTo(x, y - length);
          ctx.lineTo(x, y + length);
          ctx.stroke();
        } 
      }
    } else {
      this.positionTool = [];
      document.body.style.cursor = 'default';
    }

  }

  updateHomographicTransform(){
    const imageData = this.props.images.tree[
      this.props.images.selected_file.side
    ][this.props.images.selected_file.file]
    
    if (imageData.exif.exif.UserCommentSimplified){

      this.projectionMatrix = calculateHomographicTransform(
        [[   0. ,   0.],
        [   0. , this.image.height],
        [this.image.width, this.image.height],
        [this.image.width ,   0.]],
        [
          imageData.exif.exif.UserCommentSimplified.imgcrns.upperLeft,
          imageData.exif.exif.UserCommentSimplified.imgcrns.lowerLeft,
          imageData.exif.exif.UserCommentSimplified.imgcrns.lowerRight,
          imageData.exif.exif.UserCommentSimplified.imgcrns.upperRight
        ]
      )

    } else {
      this.projectionMatrix = null
    }
  }

  initCanvas = () => {
    this.canvasRef.width = this.canvasRef.clientWidth
    this.canvasRef.height = this.canvasRef.clientHeight
    this.canvasContext = this.canvasRef.getContext('2d')
    this.canvasContext.imageSmoothingEnabled = false
    this.canvasContext.mozImageSmoothingEnabled = false
    this.canvasContext.webkitImageSmoothingEnabled = false
    this.lastX = this.canvasRef.width / 2
    this.lastY = this.canvasRef.height / 2
    this.dragStart = null
    this.dragged = false
    trackTransforms(this.canvasContext)
  }

  imageLoaded = (file, image) => {
    const cacheName = file.side + cacheNameDivider + file.file
    this.setState(
      {
        brightness: 100,
        contrast: 100
      }
    )
    
    // reset measurement tool
    this.measurementTool = []
    this.positionTool = []
    this.projectionMatrix = null

    this.setState({
      showMeasurementTool: false,
      showPositionTool: false
    })
    if (this.image_cache.hasOwnProperty(cacheName)) {
      this.image_cache[cacheName].image = image
    }
    if (
      this.props.images.selected_file.side === file.side &&
      this.props.images.selected_file.file === file.file
    ) {
      this.image = image
      if (this.image.width === 0 || this.image.height === 0) {
        return
      }
      const canvas = this.canvasRef
      const ctx = this.canvasContext
      const neededScaleX =
        (this.image.width + 2 + 2 * this.confidenceMargin) / canvas.width
      const neededScaleY =
        (this.image.height + 2 + 2 * this.confidenceMargin) / canvas.height
      const scale = 1.0 / Math.max(neededScaleX, neededScaleY)
      ctx.scale(scale, scale)
      ctx.translate(this.confidenceMargin + 1, this.confidenceMargin + 1)

      this.canvasRedraw()
    }
  }

  loadImage = file => {
    if (file) {
      // reset state
      const cacheName = file.side + cacheNameDivider + file.file
      if (this.image_cache.hasOwnProperty(cacheName)) {
        if (this.image_cache[cacheName].hasOwnProperty('image')) {
          // console.log('Loading image from cache')
          const image = this.image_cache[cacheName].image
          this.imageLoaded(file, image)
        }
      } else {
        const image = new Image()
        image.src = `/image/${this.props.images.subfolder}/${file.side}/${file.file}`
        image.onload = () => this.imageLoaded(file, image)
        this.image_cache[cacheName] = {}
      }
    }
  }

  drawAlgoResults = () => {
    const ctx = this.canvasContext
    try {
      const selectedFile = this.props.images.tree[
        this.props.images.selected_file.side
      ][this.props.images.selected_file.file]
      const settings = this.props.images.settings

      if (this.state.showBoundingBoxes) {
        Object.entries(settings.algos.detection).forEach(
          ([algo, { normalization_threshold, threshold }], idx) => {
            if (
              selectedFile.algoResults.hasOwnProperty(algo) &&
              selectedFile.algoResults[algo].hasOwnProperty('objectDetections')
            ) {
              const bboxes = selectedFile.algoResults[algo].objectDetections[0]
              const probabilities =
                selectedFile.algoResults[algo].objectDetections[2]
              
              ctx.setLineDash([]); // Draw non dashed
              bboxes.forEach(([x1, y1, x2, y2], idx) => {
                let p = probabilities[idx]
                if (p >= threshold) {
                  p = remap_threshold(p, normalization_threshold)
                  ctx.strokeStyle = interpolateColormap(
                    p,
                    this.state.result_colors
                  )
                  ctx.lineWidth = 5
                  ctx.strokeRect(x1, y1, x2 - x1, y2 - y1)
                }
              })
            }
          }
        )
        Object.entries(settings.algos.tracking).forEach(
          ([algo, { normalization_threshold, threshold }], idx) => {
            if (
              selectedFile.algoResults.hasOwnProperty(algo) &&
              selectedFile.algoResults[algo].hasOwnProperty('tracks')
            ) {
              Object.entries(selectedFile.algoResults[algo].tracks).forEach( track => {
                const position = track[1].position;
                const box = track[1].box;
                const position_stddev_pixel = track[1].position_stddev_pixel;
                const probability = track[1].existanceProb;
                const state = track[1].state;
                const id = track[1].id;
                const detectionCount = track[1].detectionCount;
                if (probability >= threshold) {
                  var p = remap_threshold(probability, normalization_threshold);
                  ctx.strokeStyle = interpolateColormap(
                    p,
                    this.state.result_colors
                  )
                  ctx.lineWidth = 3
                  var visuText=''+id.toString()
                  if (state==='PREDICTED'){
                    ctx.setLineDash([12]);
                    visuText=visuText+'P'
                  }
                  if (state==='MEASURED'){
                    ctx.setLineDash([6]);
                    visuText=visuText+'M'
                  }
                  if (state==='VALIDATED'){
                    ctx.setLineDash([3]);
                    visuText=visuText+'V'
                  }
                  if (state==='MEASURED_VALIDATED'){
                    ctx.setLineDash([3]);
                    visuText=visuText+'V'
                  }
                  var boxTopLeftX=0;
                  var boxTopLeftY=0;
                  var boxBottomLeftX=0;
                  var boxBottomLeftY=0;
                  if(this.props.images.settings.tracksBoxSizeMode === 'positionAccuracy'){
                    const scaled_position_stddev_pixel = position_stddev_pixel*this.props.images.settings.tracksVisualizationSigma;
                    boxTopLeftX=position.x-scaled_position_stddev_pixel;
                    boxTopLeftY=position.y;
                    boxBottomLeftX=position.x-scaled_position_stddev_pixel;
                    boxBottomLeftY=position.y;
                    ctx.beginPath();
                    ctx.ellipse(
                      position.x,
                      position.y,
                      scaled_position_stddev_pixel,
                      scaled_position_stddev_pixel,
                      0,
                      0,
                      2 * Math.PI
                    )
                    ctx.stroke();
                  }
                  else if(this.props.images.settings.tracksBoxSizeMode === 'trackBoxSize'){
                    boxTopLeftX=position.x-box.width/2.0;
                    boxTopLeftY=position.y-box.height/2.0;
                    boxBottomLeftX=position.x-box.width/2.0;
                    boxBottomLeftY=position.y+box.height/2.0;
                    ctx.strokeRect(
                      boxTopLeftX,
                      boxTopLeftY,
                      box.width,
                      box.height
                    )
                  }
                  else{
                    console.error("Fallback to 'trackBoxSize' due to unsupported tracksBoxSizeMode: "+this.props.images.settings.tracksBoxSizeMode)
                    boxTopLeftX=position.x-box.width/2.0;
                    boxTopLeftY=position.y-box.height/2.0;
                    boxBottomLeftX=position.x-box.width/2.0;
                    boxBottomLeftY=position.y+box.height/2.0;
                    ctx.strokeRect(
                      boxTopLeftX,
                      boxTopLeftY,
                      box.width,
                      box.height
                    )
                  }
                  ctx.setLineDash([]); // Draw non dashed
                  ctx.font = "normal small-caps 400 30px /3 monospace";
                  ctx.strokeText(visuText, boxTopLeftX, boxTopLeftY-10)
                  ctx.strokeText(detectionCount.toString(), boxBottomLeftX, boxBottomLeftY+27)
                  }
              });
            }
          }
        )
      }

      const prob = selectedFile.probabilityWholeImage
      const color = interpolateColormap(prob, this.state.result_colors)
      ctx.strokeStyle = color
      ctx.lineWidth = this.confidenceMargin
      ctx.strokeRect(
        -this.confidenceMargin / 2 - 1,
        -this.confidenceMargin / 2 - 1,
        this.image.width + this.confidenceMargin + 2,
        this.image.height + this.confidenceMargin + 2
      )
    } catch (err) {
      console.error(err)
    }
  }

  canvasRedraw = () => {
    const canvas = this.canvasRef
    const ctx = this.canvasContext
    // Clear the entire canvas
    var p1 = ctx.transformedPoint(0, 0)
    var p2 = ctx.transformedPoint(canvas.width, canvas.height)
    ctx.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y)
    ctx.filter =  "brightness(" + this.state.brightness + "%) " +
                  "contrast(" + this.state.contrast + "%)"
    ctx.drawImage(this.image, 0, 0)
    
    this.drawAlgoResults()

    this.drawMeasurements()
    this.drawPosition()
  }

  zoom = clicks => {
    const ctx = this.canvasContext
    var pt = ctx.transformedPoint(this.lastX, this.lastY)
    ctx.translate(pt.x, pt.y)
    var factor = Math.pow(scaleFactor, clicks)
    ctx.scale(factor, factor)
    ctx.translate(-pt.x, -pt.y)
    this.canvasRedraw()
  }

  componentDidMount () {
    this.initCanvas()
    this.loadImage(this.props.images.selected_file)
    this.loadImage(this.props.images.next_file)
    this.loadImage(this.props.images.prev_file)
    document.addEventListener('keyup', this.onKeyUp)
    document.addEventListener('keydown', this.onKeyDown)
    this.canvasRef.addEventListener('mousedown', this.onMouseDown)
    this.canvasRef.addEventListener('mousemove', this.onMouseMove)
    this.canvasRef.addEventListener('mouseup', this.onMouseUp)
    this.canvasRef.addEventListener('DOMMouseScroll', this.onMouseWheel)
    this.canvasRef.addEventListener('mousewheel', this.onMouseWheel)
    this.canvasRef.addEventListener('click', this.onMouseClicked)
  }

  componentWillUnmount () {
    document.removeEventListener('keyup', this.onKeyUp)
    document.removeEventListener('keydown', this.onKeyDown)
  }

  render () {
    window.iv = this
    return (
      <div className='ImageViewer'>
        {this.props.images.settings.showCompassOverlay && (
          <OverlayCompass hidden={!this.state.showBoundingBoxes} />
        )}
        <canvas ref={ref => (this.canvasRef = ref)} />
      </div>
    )
  }
}

const mapDispatchToProps = {
  prevImage,
  nextImage,
  toggleFileFlagged,
  goToOtherSide
}
const mapStateToProps = state => ({
  images: state.images
})

export default connect(mapStateToProps, mapDispatchToProps)(ImageViewer)
