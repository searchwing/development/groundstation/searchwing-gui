import React, { useState } from 'react'
import { connect } from 'react-redux'
import './Debug.css'

function MessageSender (props) {
  const [msg, setMsg] = useState(JSON.stringify(props.body))
  return (
    <div className='MessageSender'>
      <header>{props.title}</header>
      <textarea value={msg} onChange={event => setMsg(event.target.value)} />
      <br />
      <button onClick={() => props.sendMessage(msg)}>Send</button>
    </div>
  )
}

class Debug extends React.Component {
  constructor (props) {
    super(props)
    this.prepareSocket()
  }

  prepareSocket = () => {
    this.socket = new WebSocket(`ws://${window.location.host}/ws`)

    this.socket.addEventListener('open', event => {
      console.log('Connected to backend server')
      setTimeout(
        () =>
          this.sendMessage(
            JSON.stringify({ type: 'watch_subfolder', subfolder: 'flight1' })
          ),
        1000
      )
    })
    this.socket.addEventListener('close', event => {
      setTimeout(() => this.prepareSocket(), 1000)
    })
    this.socket.addEventListener('message', event => {
      let data = JSON.parse(event.data)
      console.log(data)
    })
  }

  sendMessage = msg => {
    this.socket.send(msg)
  }

  render () {
    return (
      <div className='Debug'>
        <MessageSender
          title='Watch folder'
          body={{ type: 'watch_subfolder', subfolder: 'flight1' }}
          sendMessage={this.sendMessage}
        />
        <MessageSender
          title='View file'
          body={{
            type: 'set_viewed',
            subfolder: 'flight1',
            side: 'left',
            file: '2020-02-02T20:06:10.313277.jpg'
          }}
          sendMessage={this.sendMessage}
        />
        <MessageSender
          title='Set file as flagged'
          body={{
            type: 'set_flagged',
            subfolder: 'flight1',
            side: 'left',
            file: '2020-02-02T20:06:10.313277.jpg',
            flagged: true
          }}
          sendMessage={this.sendMessage}
        />
      </div>
    )
  }
}

const mapDispatchToProps = {}
const mapStateToProps = state => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Debug)
