import { useSelector } from 'react-redux'
import classnames from 'classnames'
import './Compass.css'

function useCompassAngle () {
  const curr_img = useSelector(state => state.images.selected_file)
  const curr_metadata = useSelector(
    state => curr_img && state.images.tree[curr_img.side][curr_img.file]
  )
  try {
    const plane_angle =
      (curr_metadata.exif.gps.GPSImgDirection * Math.PI) / 180.0
    const windrose_angle = -plane_angle
    return windrose_angle
  } catch (err) {
    return null
  }
}

export function SidebarCompass () {
  const angle = useCompassAngle()
  // https://publicdomainvectors.org/de/kostenlose-vektorgrafiken/Einfache-Windrose/55006.html
  // -> https://openclipart.org/detail/173629/simple-wind-rose
  return (
    <div className='SidebarCompass'>
      {angle && (
        <img
          alt=''
          style={{ transform: `rotate(${angle}rad)` }}
          src='/simple-wind-rose.svg'
        />
      )}
    </div>
  )
}

export function OverlayCompass ({ hidden }) {
  const angle = useCompassAngle()
  const opacity = useSelector(
    state => state.images.settings.compassOverlayOpacity
  )
  // https://openclipart.org/detail/325034/north-arrow-for-maps
  return (
    <div
      className={classnames('OverlayCompass', { hidden })}
      style={{ opacity }}
    >
      {angle && (
        <img
          alt=''
          style={{ transform: `rotate(${angle}rad)` }}
          src='/north-arrow-for-maps.svg'
        />
      )}
    </div>
  )
}
