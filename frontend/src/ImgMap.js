import React from 'react'
import { connect } from 'react-redux'
import 'react-treeview/react-treeview.css'
import './ImgMap.css'

class ImgMap extends React.Component {
  constructor (props) {
    super(props)
    this.normed_coors = []
    this.circleRadius = 6
    this.min_lat = 0
    this.max_lat = 0
    this.min_lon = 0
    this.max_lat = 0
  }

  getPaddedX = x => (x * 0.9 + 0.05) * this.canvas.width
  getPaddedY = y => (1.0 - y * 0.9 - 0.05) * this.canvas.height

  normalizeCoordinate = (lat, lon) => {
    return {
      x: (lon - this.min_lon + this.eps) / (this.max_lon - this.min_lon + this.eps),
      y: (lat - this.min_lat + this.eps) / (this.max_lat - this.min_lat + this.eps)
    }
  }


  computeNormedCoors = () => {
    const img_list = this.props.images.sidebar_list_datetime_sorted.left
    const simple_coors = []
    this.max_lat = -Infinity
    this.min_lat = Infinity
    this.max_lon = -Infinity
    this.min_lon = Infinity
    img_list.forEach(([file, metadata]) => {
      if (
        metadata &&
        metadata.exif &&
        metadata.exif.gps &&
        metadata.exif.gps.simplifiedLatLon
      ) {
        const latlon_raw = metadata.exif.gps.simplifiedLatLon
        const loc = {
          lon: parseFloat(latlon_raw.lon.substr(0, latlon_raw.lon.length - 1)),
          lat: parseFloat(latlon_raw.lat.substr(0, latlon_raw.lat.length - 1))
        }
        // We are not checking for N/S or W/E because this has been taken care of in the backend
        // If both values are basically zero, the data is probably wrong maaan
        if (!(loc.lon < 1 && loc.lon > -1) || !(loc.lat < 1 && loc.lat > -1)) {
          this.max_lat = Math.max(loc.lat, this.max_lat)
          this.min_lat = Math.min(loc.lat, this.min_lat)
          this.max_lon = Math.max(loc.lon, this.max_lon)
          this.min_lon = Math.min(loc.lon, this.min_lon)
          simple_coors.push(loc)
        }
      }
    })

    this.eps = 1e-7
    this.normed_coors = []
    simple_coors.forEach(coor => {
      this.normed_coors.push({
        x:
          (coor.lon - this.min_lon + this.eps) / (this.max_lon - this.min_lon + this.eps),
        y: (coor.lat - this.min_lat + this.eps) / (this.max_lat - this.min_lat + this.eps)
      })
    })

    this.normedLeftImageAreas = []
    this.normedRightImageAreas = []
    this.normedViewedImageAreas = []
    this.normedFilteredImageAreas = []

    this.props.images.sidebar_list_datetime_sorted.left.forEach(([file, metadata]) => {
      const imageArea = this.convertImageAreaToNormalizedCoordinates(metadata)
      if (imageArea) {
        this.normedLeftImageAreas.push(imageArea)
        if (metadata.manual.views > 0)
        {
          this.normedViewedImageAreas.push(imageArea)
        }
      }
    }
    )

    this.props.images.sidebar_list_datetime_sorted.right.forEach(([file, metadata]) => {
      const imageArea = this.convertImageAreaToNormalizedCoordinates(metadata)
      if (imageArea) {
        this.normedRightImageAreas.push(imageArea)
        if (metadata.manual.views > 0)
        {
          this.normedViewedImageAreas.push(imageArea)
        }
      }
    }
    )

    this.props.images.sidebar_list.left.concat(this.props.images.sidebar_list.right).forEach(([file, metadata]) => {
      const imageArea = this.convertImageAreaToNormalizedCoordinates(metadata)
      if (imageArea) {
        this.normedFilteredImageAreas.push(imageArea)
      }
      }
    )
  }

  convertImageAreaToNormalizedCoordinates = (metadata) => {
    if (
      metadata.exif &&
      metadata.exif.exif &&
      metadata.exif.exif.UserCommentSimplified &&
      metadata.exif.exif.UserCommentSimplified.imgcrns
    ) {
        const lowerLeft = metadata.exif.exif.UserCommentSimplified.imgcrns.lowerLeft
        const lowerRight = metadata.exif.exif.UserCommentSimplified.imgcrns.lowerRight
        const upperLeft = metadata.exif.exif.UserCommentSimplified.imgcrns.upperLeft
        const upperRight = metadata.exif.exif.UserCommentSimplified.imgcrns.upperRight
        
        return {
          lowerLeft: this.normalizeCoordinate(
            lowerLeft[0],
            lowerLeft[1],
          ),
          lowerRight: this.normalizeCoordinate(
            lowerRight[0],
            lowerRight[1]
          ),
          upperRight: this.normalizeCoordinate(
            upperRight[0],
            upperRight[1],
          ),
          upperLeft: this.normalizeCoordinate(
            upperLeft[0],
            upperLeft[1],
        )
      }
    }
  }
  
  plotImageArea = (area, ctx, style, fill = true) => {
    if (fill) {ctx.fillStyle = style}
    else {ctx.strokeStyle = style}

    ctx.beginPath();
    ctx.moveTo(this.getPaddedX(area.upperLeft.x), this.getPaddedY(area.upperLeft.y));
    ctx.lineTo(this.getPaddedX(area.upperRight.x), this.getPaddedY(area.upperRight.y));
    ctx.lineTo(this.getPaddedX(area.lowerRight.x), this.getPaddedY(area.lowerRight.y));
    ctx.lineTo(this.getPaddedX(area.lowerLeft.x), this.getPaddedY(area.lowerLeft.y));
    ctx.closePath();
    if (fill) {ctx.fill()}
    else {ctx.stroke()} 
  }


  plotMap = () => {
    const ctx = this.canvas.getContext('2d')
    ctx.fillStyle = '#006994'
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)
    if (Object.keys(this.normed_coors).length === 0) {
      return
    }

    if (this.props.images.settings.showAllImageAreasOnMap){
      this.normedRightImageAreas.forEach(area => {
        this.plotImageArea(area, ctx, 'rgba(' + this.props.images.settings.allImageAreasOnMapColor.join() + ',1)')
      })

      this.normedLeftImageAreas.forEach(area => {
        this.plotImageArea(area, ctx, 'rgba(' + this.props.images.settings.allImageAreasOnMapColor.join() + ',1)')
      })
    }

    if (this.props.images.settings.showFilteredImageAreasOnMap) {
      this.normedFilteredImageAreas.forEach(area => {
        this.plotImageArea(area, ctx, 'rgba(' + this.props.images.settings.filteredImageAreasOnMapColor.join() + ',1)')
      })
    }

    if (this.props.images.settings.showVisitedImagesOnMap){
      this.normedViewedImageAreas.forEach(area => {
      this.plotImageArea(area, ctx, 'rgba(' + this.props.images.settings.visitedAreasOnMapColor.join() + ',1)')
    })
    }

    ctx.strokeStyle = 'white'
    ctx.lineWidth = 1
    ctx.beginPath()
    ctx.moveTo(
      this.getPaddedX(this.normed_coors[0].x),
      this.getPaddedY(this.normed_coors[0].y)
    )
    this.normed_coors.forEach(coor => {
      ctx.lineTo(this.getPaddedX(coor.x), this.getPaddedY(coor.y))
    })
    ctx.stroke()
  }

  plotCurrentPosition = () => {
    if (this.props.images.selected_file === null) {
      return
    }
    try {
      const ctx = this.canvas.getContext('2d')

      const curr_img = this.props.images.selected_file
      const curr_metadata = this.props.images.tree[curr_img.side][curr_img.file]

      if (!curr_metadata) {
        console.log("I'm not sure about this whole thing")
        return
      }
      
      // Image area
      if (this.props.images.settings.showNextImageAreaOnMap) {
        const nextImg = this.props.images.next_file
        const nextImgMeta = this.props.images.tree[nextImg.side][nextImg.file]
        const nextAreaCovered = this.convertImageAreaToNormalizedCoordinates(nextImgMeta)
        if (nextAreaCovered){
          this.plotImageArea(nextAreaCovered, ctx, 'rgba(' + this.props.images.settings.nextImageAreaOnMapColor.join() + ')', false)
        }
      }

      if (this.props.images.settings.showCurrentImageAreaOnMap) {
        const currAreaCovered = this.convertImageAreaToNormalizedCoordinates(curr_metadata)
        if (currAreaCovered){
          this.plotImageArea(currAreaCovered, ctx, 'rgba(' + this.props.images.settings.currentImageAreaOnMapColor.join() + ',1)', false)
        }
      }

      const eps = 1e-7

      // Home GPS position
      try {
        if ("homePos" in curr_metadata.exif.exif.UserCommentSimplified){
          const home_latlon_raw = curr_metadata.exif.exif.UserCommentSimplified.homePos
          var home_loc = {
            x:
              (parseFloat(home_latlon_raw.longitude) - //
                this.min_lon +
                eps) /
              (this.max_lon - this.min_lon + eps),
    
            y:
              (parseFloat(home_latlon_raw.latitude) - 
                this.min_lat +
                eps) /
              (this.max_lat - this.min_lat + eps)
          }
          var img = new Image();
          //https://openclipart.org/detail/190960/anchor
          img.src = '/anchor.png';
          var scale = 0.05;
          var anchorX = 177;
          var anchorY = 345;
          var width = img.width*scale;
          var height = img.height*scale;
          var x = this.getPaddedX(home_loc.x) - anchorX*scale;
          var y = this.getPaddedY(home_loc.y) - anchorY*scale;
          ctx.drawImage(img, x,y, width, height);
        } 
      }
      catch(err){}
      

      // Drone GPS position
      ctx.beginPath()
      ctx.fillStyle = 'yellow'
      const latlon_raw = curr_metadata.exif.gps.simplifiedLatLon
      var drone_loc = {
        x:
          (parseFloat(latlon_raw.lon.substr(0, latlon_raw.lon.length - 1)) -
            this.min_lon +
            eps) /
          (this.max_lon - this.min_lon + eps),

        y:
          (parseFloat(latlon_raw.lat.substr(0, latlon_raw.lat.length - 1)) -
            this.min_lat +
            eps) /
          (this.max_lat - this.min_lat + eps)
      }
      ctx.arc(
        this.getPaddedX(drone_loc.x),
        this.getPaddedY(drone_loc.y),
        this.circleRadius,
        0,
        2 * Math.PI
      )
      ctx.fill()
      // Getting current angle
      const angle =
        (curr_metadata.exif.gps.GPSImgDirection * Math.PI) / 180 - 0.5 * Math.PI
      const camera_angle =
        curr_img.side === 'left' ? angle - Math.PI / 2 : angle + Math.PI / 2
      ctx.beginPath()
      ctx.strokeStyle = 'red'
      ctx.lineWidth = 3
      ctx.beginPath()
      ctx.moveTo(this.getPaddedX(drone_loc.x), this.getPaddedY(drone_loc.y))
      ctx.lineTo(
        this.getPaddedX(drone_loc.x) + Math.cos(angle) * this.circleRadius,
        this.getPaddedY(drone_loc.y) + Math.sin(angle) * this.circleRadius
      )
      ctx.stroke()
      ctx.fill()
      ctx.beginPath()
      ctx.strokeStyle = 'grey'
      ctx.lineWidth = 2
      ctx.beginPath()
      ctx.moveTo(this.getPaddedX(drone_loc.x), this.getPaddedY(drone_loc.y))
      ctx.lineTo(
        this.getPaddedX(drone_loc.x) + Math.cos(camera_angle) * this.circleRadius,
        this.getPaddedY(drone_loc.y) + Math.sin(camera_angle) * this.circleRadius
      )
      ctx.stroke()
      ctx.fill()
    } catch (err) {}
  }
  componentDidMount () {
    this.canvas.width = this.canvas.parentNode.clientWidth
    if (this.props.images.sidebar_list_datetime_sorted.left.length > 0) {
      this.computeNormedCoors()
      this.plotMap()
      this.plotCurrentPosition()
    }
  }
  componentDidUpdate (prevProps, prevState) {
    if (
      (prevProps.images.sidebar_list_datetime_sorted.left !==
      this.props.images.sidebar_list_datetime_sorted.left) ||
      (this.props.images.settings.showAllImageAreasOnMap !== prevProps.images.settings.showAllImageAreasOnMap) ||
      (this.props.images.settings.showVisitedImagesOnMap !== prevProps.images.settings.showVisitedImagesOnMap) ||
      (this.props.images.settings.showCurrentImageAreaOnMap !== prevProps.images.settings.showCurrentImageAreaOnMap) ||
      (this.props.images.settings.showNextImageAreaOnMap !== prevProps.images.settings.showNextImageAreaOnMap) ||
      (this.props.images.settings.showFilteredImageAreasOnMap !== prevProps.images.settings.showFilteredImageAreasOnMap)
    ) {
      this.computeNormedCoors()
      this.plotMap()
      this.plotCurrentPosition()
    } else if (
      prevProps.images.selected_file !== this.props.images.selected_file
    ) {
      this.plotMap()
      this.plotCurrentPosition()
    }
  }

  render () {
    return (
      <div className='Map'>
        <canvas
          ref={ref => (this.canvas = ref)}
          height={200}
        />
      </div>
    )
  }
}

const mapDispatchToProps = {}
const mapStateToProps = state => ({
  images: state.images
})

export default connect(mapStateToProps, mapDispatchToProps)(ImgMap)
