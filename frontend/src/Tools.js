// from https://stackoverflow.com/a/5786281
export function ConvertDDToDMS(D, lng) {
    return {
      dir: D < 0 ? (lng ? "W" : "S") : lng ? "E" : "N",
      deg: 0 | (D < 0 ? (D = -D) : D),
      min: 0 | (((D += 1e-9) % 1) * 60),
      sec: (0 | (((D * 60) % 1) * 6000)) / 100,
    };
  }
  
export function ConvertDDToDMSString(D, lng) {
    const DMS=ConvertDDToDMS(D,lng)
    return(DMS.deg+"° "+DMS.min+"' "+DMS.sec+"'' "+DMS.dir)
  }
  