import { connect } from 'react-redux'
import { selectSubfolder } from './reducers/imagesReducer.js'
import './Home.css'

function Home (props) {
  const subfolderList = props.subfolders.subfolders.map(subfolder => (
    <div
      key={subfolder.subfolder}
      className='subfolder'
      onClick={() => props.selectSubfolder(subfolder.subfolder)}
    >
      {subfolder.subfolderText}
    </div>
  ))
  return (
    <div className='Home'>
      <header>Select a folder</header>
      <main>{subfolderList}</main>
    </div>
  )
}

const mapDispatchToProps = {
  selectSubfolder
}
const mapStateToProps = state => ({
  subfolders: state.subfolders,
  connection: state.connection,
  subfoldersText: state.subfoldersText
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
