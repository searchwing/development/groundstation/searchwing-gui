import React from 'react'
import { connect, useSelector } from 'react-redux'
import {
  selectFile,
  setFilterFunction,
  setSortFunction,
  setCycleMode,
  setSkipSeconds
} from './reducers/imagesReducer.js'
import { interpolateColormap } from './ImageViewer'
import classnames from 'classnames'
import ExifInspector from './ExifInspector'
import Collapsable from './Collapsable'
import ImgMap from './ImgMap'
import { SidebarCompass } from './Compass'
// import TreeView from 'react-treeview'
import 'react-treeview/react-treeview.css'
import './Sidebar.css'

function makeEmojiIndicator (num, str) {
  return (
    <div className='viewedCounter'>
      {num > 0 && (
        <React.Fragment>
          <span
            role='img'
            area-label='silence-warning'
            aria-labelledby='silence-warning'
            className='viewedIcon'
          >
            {str}
          </span>
          {num}
        </React.Fragment>
      )}
    </div>
  )
}

function SidebarElementProbabilityIndicator ({ probability, shape }) {
  const result_colors = useSelector(
    state => state.images.settings.result_colors
  )
  return (
    <div
      className={classnames('probBox', { [shape]: true })}
      style={{
        backgroundColor:
          probability === -1
            ? 'black'
            : interpolateColormap(probability, result_colors),
        opacity: probability === -1 ? 0.05 : 1.0
      }}
    />
  )
}

class SidebarElement extends React.Component {
  constructor (props) {
    super(props)
    this.el = null
  }
  isVisible = () => {
    if (!this.el || !this.el.parentElement) {
      return true
    }
    const { top, bottom, height } = this.el.getBoundingClientRect()
    const parentRect = this.el.parentElement.getBoundingClientRect()
    const isVisible =
      top <= parentRect.top
        ? parentRect.top - top < height - 1
        : bottom - parentRect.bottom < height - 1
    return isVisible
  }
  shouldComponentUpdate (nextProps, nextState) {
    const wasOrIsSelected =
      this.props.selectedFile !== nextProps.selectedFile &&
      ((this.props.selectedFile &&
        this.props.selectedFile.side === this.props.side &&
        this.props.selectedFile.file === this.props.file) ||
        (nextProps.selectedFile &&
          nextProps.selectedFile.side === this.props.side &&
          nextProps.selectedFile.file === this.props.file))
    const dataHasChanged = this.props.metadata !== nextProps.metadata
    const anchorHasChanged = this.props.isAnchor !== nextProps.isAnchor
    const settingsHaveChanged = this.props.settings !== nextProps.settings
    return (
      wasOrIsSelected ||
      dataHasChanged ||
      anchorHasChanged ||
      settingsHaveChanged
    )
  }
  componentDidUpdate (prevProps, prevState) {
    const isSelected =
      this.props.selectedFile &&
      this.props.selectedFile.side === this.props.side &&
      this.props.selectedFile.file === this.props.file
    if (isSelected) {
      if (this.el && !this.isVisible()) {
        this.el.scrollIntoView({ block: 'nearest' })
      }
    }
  }
  render () {
    const metadata = this.props.metadata
    const settings = this.props.settings
    return (
      <div
        className={classnames('SidebarElement', {
          selected:
            this.props.selectedFile &&
            this.props.selectedFile.side === this.props.side &&
            this.props.selectedFile.file === this.props.file,
          flagged: metadata.manual.flagged,
          anchor: this.props.isAnchor
        })}
        ref={ref => (this.el = ref)}
        onClick={() => this.props.onClick(this.props.side, this.props.file)}
      >
        <div title={this.props.file} className='label'>
          {metadata.timeLabel}
        </div>
        {metadata.manual.flagged && <div>&#9873;</div>}
        {metadata.manual.flagged || (
          <>
            {!settings.showSeparateAlgoIndicators && (
              <>
                <SidebarElementProbabilityIndicator
                  probability={metadata.probabilityBboxes}
                  shape='square'
                />
                <SidebarElementProbabilityIndicator
                  probability={metadata.probabilityWholeImage}
                  shape='circle'
                />
              </>
            )}
            {settings.showSeparateAlgoIndicators && (
              <>
                {metadata.bbox_probabilities.map((p, idx) => (
                  <SidebarElementProbabilityIndicator
                    key={`bbox_algo_${idx}`}
                    probability={p}
                    shape='square'
                  />
                ))}
                {metadata.tracked_bbox_probabilities.map((p, idx) => (
                  <SidebarElementProbabilityIndicator
                    key={`bbox_algo_${idx}`}
                    probability={p}
                    shape='circle'
                  />
                ))}
              </>
            )}
          </>
        )}
        {makeEmojiIndicator(metadata.manual.views || 0, '👀')}
      </div>
    )
  }
}

function Sidebar (props) {
  const makeFileList = (sidebarList, side, anchor) =>
    sidebarList.map(([file, metadata]) => (
      <SidebarElement
        key={file}
        side={side}
        file={file}
        metadata={metadata}
        settings={props.images.settings}
        selectedFile={props.images.selected_file}
        isAnchor={anchor !== null && file === anchor.file}
        onClick={props.selectFile}
      />
    ))
  const fileListLeft = makeFileList(
    props.images.sidebar_list.left,
    'left',
    props.images.anchors.left
  )
  const fileListRight = makeFileList(
    props.images.sidebar_list.right,
    'right',
    props.images.anchors.right
  )
  return (
    <div className='Sidebar'>
      <div className='ViewOptions'>
        <section>
          <header>Skip n seconds:</header>
          <input
            className='skipSeconds'
            type='number'
            min='0'
            max='20'
            onChange={event => props.setSkipSeconds(event.target.value)}
            value={props.images.settings.skip_seconds}
          />
        </section>
        <section>
          <header>Sort:</header>
          <select
            onChange={event => props.setSortFunction(event.target.value)}
            value={props.images.settings.sidebar_sort_function}
          >
            <option value='filename_sort_func'>Filename</option>
            <option value='datetime_sort_func'>DateTime</option>
            <option value='viewed_sort_func'>Views</option>
            <option value='algo_sort_func'>Algorithm</option>
          </select>
        </section>
        <section>
          <header>Filter:</header>
          <select
            onChange={event => props.setFilterFunction(event.target.value)}
            value={props.images.settings.sidebar_filter_function}
          >
            <option value='no_filter_func'>All</option>
            <option value='view_filter_func'>Not viewed</option>
            <option value='flagged_filter_func'>Flagged </option>
          </select>
        </section>
        <section>
          <header>Cycle Mode:</header>
          <select
            onChange={event => props.setCycleMode(event.target.value)}
            value={props.images.settings.cycle_mode}
            disabled={
              props.images.settings.sidebar_sort_function !==
              'datetime_sort_func'
            }
          >
            <option value='stay_on_side'>Stay on side</option>
            {props.images.settings.sidebar_sort_function ===
              'datetime_sort_func' && (
              <React.Fragment>
                <option value='alternate_sides'>Alternate sides</option>
                <option value='smart_cycle'>Smart Cycle</option>
                <option value='filebrowser'>File Browser</option>
              </React.Fragment>
            )}
          </select>
        </section>
      </div>
      <div className='FileTree'>
        <section>{fileListLeft}</section>
        <section>{fileListRight}</section>
      </div>
      <Collapsable title='Map'>
        <ImgMap />
      </Collapsable>
      <Collapsable title='Compass'>
        <SidebarCompass />
      </Collapsable>
      <Collapsable title='Metadata Inspector' collapsed>
        <ExifInspector />
      </Collapsable>
    </div>
  )
}

const mapDispatchToProps = {
  selectFile,
  setFilterFunction,
  setSortFunction,
  setCycleMode,
  setSkipSeconds
}
const mapStateToProps = state => ({
  images: state.images
})

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
