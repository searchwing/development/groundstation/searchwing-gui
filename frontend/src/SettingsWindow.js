import React from 'react'
import { connect } from 'react-redux'
import './SettingsWindow.css'
import { setSettings } from './reducers/imagesReducer'

class SettingsWindow extends React.Component {
  constructor (props) {
    super(props)
    this.state = { settings: props.settings }
    this.originalState = this.state
  }

  setOrdinalValue = settingsKey => event => {
    this.setState({
      ...this.state,
      settings: {
        ...this.state.settings,
        [settingsKey]: event.target.value
      }
    })
  }

  setFloatValue = settingsKey => event => {
    this.setState({
      ...this.state,
      settings: {
        ...this.state.settings,
        [settingsKey]: parseFloat(event.target.value)
      }
    })
  }

  setBoolValue = settingsKey => event => {
    this.setState({
      ...this.state,
      settings: {
        ...this.state.settings,
        [settingsKey]: event.target.checked
      }
    })
  }

  componentWillUnmount () {
    if (this.originalState !== this.state) {
      this.props.setSettings(this.state.settings)
    }
  }

  render () {
    return (
      <main className='SettingsWindow'>
        <header>Settings</header>
        <div className='SectionContainer'>
          <section>
            <header>
              Bounding Box behavior
              <span className='summary'>
                Hide bounding boxes by default, and show them when shift is
                pressed - or the other way around
              </span>
            </header>
            <main>
              <select
                onChange={this.setOrdinalValue('boundingBoxBehavior')}
                value={this.state.settings.boundingBoxBehavior}
              >
                <option value='hideWithShift'>Hide with Shift</option>
                <option value='showWithShift'>Show with Shift</option>
              </select>
            </main>
          </section>
          <section>
            <header>
              Smart Cycle time
              <span className='summary'>
                The maximum time difference after which the next image is chosen
                from the other side's camera
              </span>
            </header>
            <main>
              <input
                className='skipSeconds'
                type='number'
                min='0'
                max='20'
                onChange={this.setFloatValue('smartCycleMaxTimeDiff')}
                value={this.state.settings.smartCycleMaxTimeDiff}
              />
            </main>
          </section>
          <section>
            <header>
              Compass overlay
              <span className='summary'>Show compass overlay on image</span>
            </header>
            <main>
              <input
                className='showCompassOverlay'
                type='checkbox'
                onChange={this.setBoolValue('showCompassOverlay')}
                checked={this.state.settings.showCompassOverlay}
              />
            </main>
          </section>
          <section>
            <header>
              Show separate algorithm indicators
              <span className='summary'>
                Show a colored square/circle in the sidebar for every algorithm.
                If disabled, only one is shown for detection algorithms and one
                for classification algorithm, each indicating their maximum
                probability
              </span>
            </header>
            <main>
              <input
                className='showCompassOverlay'
                type='checkbox'
                onChange={this.setBoolValue('showSeparateAlgoIndicators')}
                checked={this.state.settings.showSeparateAlgoIndicators}
              />
            </main>
          </section>
          <section>
            <header>
              Highlight visited images on map
            </header>
            <main>
              <input
                className='showVisitedImagesOnMap'
                type='checkbox'
                onChange={this.setBoolValue('showVisitedImagesOnMap')}
                checked={this.state.settings.showVisitedImagesOnMap}
              />
            </main>
          </section>
          <section>
            <header>
              Highlight left/right image area on map
            </header>
            <main>
              <input
                className='showAllImageAreasOnMap'
                type='checkbox'
                onChange={this.setBoolValue('showAllImageAreasOnMap')}
                checked={this.state.settings.showAllImageAreasOnMap}
              />
            </main>
          </section>
          <section>
            <header>
              Highlight filtered image area on map
            </header>
            <main>
              <input
                className='showFilteredImageAreasOnMap'
                type='checkbox'
                onChange={this.setBoolValue('showFilteredImageAreasOnMap')}
                checked={this.state.settings.showFilteredImageAreasOnMap}
              />
            </main>
          </section>
          <section>
            <header>
              Highlight current image area on map
            </header>
            <main>
              <input
                className='showCurrentImageAreaOnMap'
                type='checkbox'
                onChange={this.setBoolValue('showCurrentImageAreaOnMap')}
                checked={this.state.settings.showCurrentImageAreaOnMap}
              />
            </main>
          </section>
          <section>
            <header>
              Highlight next image area in map
            </header>
            <main>
              <input
                className='showNextImageAreaOnMap'
                type='checkbox'
                onChange={this.setBoolValue('showNextImageAreaOnMap')}
                checked={this.state.settings.showNextImageAreaOnMap}
              />
            </main>
          </section>
        </div>
      </main>
    )
  }
}

const mapDispatchToProps = { setSettings }
const mapStateToProps = state => ({ settings: state.images.settings })

export default connect(mapStateToProps, mapDispatchToProps)(SettingsWindow)
