export function setSubfolders (subfolders) {
  return {
    type: 'SET_SUBFOLDERS',
    subfolders: subfolders
  }
}

const subfolders = (
  state = {
    subfolders: []
  },
  action
) => {
  switch (action.type) {
    case 'SET_SUBFOLDERS':
      return { ...state, subfolders: action.subfolders }

    default:
      return state
  }
}

export default subfolders
