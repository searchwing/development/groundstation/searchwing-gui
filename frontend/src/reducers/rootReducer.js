import { combineReducers } from 'redux'
import subfolders, { setSubfolders } from './subfolderReducer.js'
import images, {
  setSettings,
  selectSubfolder,
  receiveLeftRightAllFiles,
  receiveChangedFiles,
  receiveChangedMetadata
} from './imagesReducer.js'

// Holds the connection to the backend
window.socket = null

export function connectToBackend () {
  return async dispatch => {
    prepareSocket(dispatch)
  }
}

function prepareSocket (dispatch) {
  window.socket = new WebSocket(`ws://${window.location.host}/ws`)

  window.socket.addEventListener('open', function (event) {
    // window.socket.send('Hello Server!');
    console.log('Connected to backend server')
    dispatch(setConnected('connected'))
  })

  window.socket.addEventListener('close', function (event) {
    dispatch(setConnected('lost_connection'))
    // console.error('onclose')
    console.warn('Socket connection closed, trying to reconnect')
    dispatch(selectSubfolder(null))
    setTimeout(() => prepareSocket(dispatch), 1000)
  })

  window.socket.addEventListener('message', function (event) {
    let data = JSON.parse(event.data)
    console.warn('onmessage', data)
    if (data.type === 'subfolders') {
      // TODO: Set subfolder to null if it was removed
      dispatch(setSubfolders(data.subfolders))
    } else if (data.type === 'left_right_all_files') {
      if (data.left && data.right) {
        dispatch(receiveLeftRightAllFiles(data.left, data.right))
      }
    } else if (data.type === 'changed_files') {
      if (
        data.subfolder &&
        data.side &&
        data.removedFiles &&
        data.newAlgoData &&
        data.newFileData
      ) {
        dispatch(
          receiveChangedFiles(
            data.subfolder,
            data.side,
            data.removedFiles,
            data.newAlgoData,
            data.newFileData
          )
        )
      }
    } else if (data.type === 'manual_data_changed') {
      if (data.subfolder && data.side && data.file && data.changedData) {
        dispatch(
          receiveChangedMetadata(
            data.subfolder,
            data.side,
            data.file,
            data.changedData
          )
        )
      } 
      // } else if (data.type === 'full_tree') {
      //   dispatch(setFullTree(data.tree))
      // } else if (data.type === 'new_file') {
      //   dispatch(addNewFile(data.folder, data.file, data.metadata))
      // } else if (data.type === 'update_file_metadata') {
      //   dispatch(updateFileMetadata(data.folder, data.file, data.metadata))
    }
    else if (data.type === 'frontendSettings') {
      dispatch(setSettings(data.settings))
    }
  })
}

export function setConnected (connected) {
  return {
    type: 'SET_CONNECTED',
    connected: connected
  }
}

const connection = (
  state = {
    connected: 'not_yet_connected'
  },
  action
) => {
  switch (action.type) {
    case 'SET_CONNECTED':
      return { ...state, connected: action.connected }

    default:
      return state
  }
}

export default combineReducers({
  connection,
  images,
  subfolders
})
