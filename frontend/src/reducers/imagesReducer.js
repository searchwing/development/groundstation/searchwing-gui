// Utility function to map dictionaries onto other dictionaries
Object.map = (object, mapFn) => {
  return Object.keys(object).reduce(function (result, key) {
    result[key] = mapFn(object[key])
    return result
  }, {})
}

// Utility function to filter dictionaries by key
Object.filter = (obj, predicate) =>
  Object.keys(obj)
    .filter(key => predicate(key, obj[key]))
    .reduce((res, key) => Object.assign(res, { [key]: obj[key] }), {})

Array.findLastIndex = arr => cond => {
  if (!arr.length) return -1
  if (!cond) return arr.length - 1
  for (var i = arr.length - 1; i >= 0; --i) {
    if (cond(arr[i])) return i
  }
  return -1
}
Array.findFirstOrLastIndex = (arr, first, cond) => {
  if (first) {
    return arr.findIndex(cond)
  }
  if (!arr.length) return -1
  if (!cond) return arr.length - 1
  for (var i = arr.length - 1; i >= 0; --i) {
    if (cond(arr[i])) return i
  }
  return -1
}

function clip (num, min, max) {
  return Math.min(max, Math.max(num, min))
}

export function remap_threshold (p, normalization_threshold_logit) {
  return (
    1 /
    (1 +
      Math.exp(
        -(Math.log(p / Math.max(1 - p, 0)) + normalization_threshold_logit)
      ))
  )
}

function otherSide (side) {
  return side === 'left' ? 'right' : 'left'
}

function socket_send (msg) {
  try {
    window.socket.send(JSON.stringify(msg))
  } catch (err) {
    console.error(err)
  }
}

export function selectSubfolder (subfolder) {
  return {
    type: 'SELECT_SUBFOLDER',
    subfolder: subfolder
  }
}
export function subscribeToImages (subfolder) {
  return async dispatch => {
    socket_send({ type: 'watch_subfolder', subfolder: subfolder })
  }
}
export function unsubscribeFromImages () {
  return async dispatch => {
    socket_send({ type: 'unwatch_subfolder' })
  }
}
export function receiveLeftRightAllFiles (left, right) {
  return {
    type: 'RECEIVE_LEFT_RIGHT_ALL_FILES',
    left: left,
    right: right
  }
}
export function receiveChangedFiles (
  subfolder,
  side,
  removedFiles,
  newAlgoData,
  newFileData
) {
  return {
    type: 'RECEIVE_CHANGED_FILES',
    subfolder: subfolder,
    side: side,
    removedFiles: removedFiles,
    newAlgoData: newAlgoData,
    newFileData: newFileData
  }
}
export function receiveChangedMetadata (subfolder, side, file, changedData) {
  return {
    type: 'RECEIVE_CHANGED_METADATA',
    subfolder: subfolder,
    side: side,
    file: file,
    changedData: changedData
  }
}

export function selectFile (side, file) {
  return {
    type: 'SELECT_FILE',
    side: side,
    file: file
  }
}
export function setFileViewed (subfolder, side, file) {
  socket_send({
    type: 'set_viewed',
    subfolder: subfolder,
    side: side,
    file: file
  })
}
export function toggleFileFlagged (subfolder, side, file) {
  socket_send({
    type: 'toggle_flagged',
    subfolder: subfolder,
    side: side,
    file: file
  })
}

export function nextImage () {
  return {
    type: 'NEXT_IMAGE'
  }
}

export function prevImage () {
  return {
    type: 'PREV_IMAGE'
  }
}

export function goToOtherSide () {
  return {
    type: 'GO_TO_OTHER_SIDE'
  }
}

// export function setLabelFunction (label_func) {
//   return {
//     type: 'SET_LABEL_FUNC',
//     label_func: label_func
//   }
// }
export function setFilterFunction (filter_func) {
  return {
    type: 'SET_FILTER_FUNC',
    filter_func: filter_func
  }
}

export function setSkipSeconds (skip_seconds) {
  return {
    type: 'SET_SKIP_SECONDS',
    skip_seconds: skip_seconds
  }
}
export function setSortFunction (sort_func) {
  return {
    type: 'SET_SORT_FUNC',
    sort_func: sort_func
  }
}
export function setCycleMode (cycle_mode) {
  return {
    type: 'SET_CYCLE_MODE',
    cycle_mode: cycle_mode
  }
}
export function filename_sort_func (
  [filename1, metadata1],
  [filename2, metadata2]
) {
  return filename1.localeCompare(filename2) // Sort by filename
}
export function datetime_sort_func (
  [filename1, metadata1],
  [filename2, metadata2]
) {
  // try {
  //   const mom1 = moment(metadata1.exif.exif.DateTimeOriginal)
  //   const mom2 = moment(metadata2.exif.exif.DateTimeOriginal)
  //   return mom1.valueOf() - mom2.valueOf()
  // } catch (err) {
  //   return -1 // Sort all without exifdata at bottom?
  // }
  return metadata1.timestamp - metadata2.timestamp
}
export function viewed_sort_func (
  [filename1, metadata1],
  [filename2, metadata2]
) {
  return (metadata1.manual.views || 0) - (metadata2.manual.views || 0)
}
export function algo_sort_func (
  [filename1, metadata1],
  [filename2, metadata2]
) {
  return metadata2.probability - metadata1.probability
}
export const sort_funcs = {
  filename_sort_func: filename_sort_func,
  datetime_sort_func: datetime_sort_func,
  viewed_sort_func: viewed_sort_func,
  algo_sort_func: algo_sort_func
}
export function setSettings (settings) {
  return {
    type: 'SET_SETTINGS',
    settings
  }
}
// export function filename_label_func ([filename, metadata]) {
//   return [filename, filename, metadata]
// }
// export function datetime_label_func ([filename, metadata]) {
//   let label = filename
//   try {
//     label = moment(metadata.exif.exif.DateTimeOriginal).format(
//       'DD.MM.YYYY - HH:mm:ss'
//     )
//   } catch (err) {}
//   return [label, filename, metadata]
// }
// export const label_funcs = {
//   filename_label_func: filename_label_func,
//   datetime_label_func: datetime_label_func
// }

export function view_filter_func ([image, metadata]) {
  return (metadata.manual.views || 0) === 0
}
export function flagged_filter_func ([image, metadata]) {
  return metadata.manual.flagged
}
export function no_filter_func ([image, metadata]) {
  return true
}
function skip_filter_func (sorted_list, tree, skip_seconds) {
  if (!skip_seconds || sorted_list.length === 0) {
    return tree
  }
  let trueList = { [sorted_list[0][0]]: tree[sorted_list[0][0]] }
  let timeAnchor = sorted_list[0][1].timestamp
  let counter = 1
  while (counter < sorted_list.length) {
    let c_time = sorted_list[counter][1].timestamp
    if (c_time - timeAnchor >= skip_seconds) {
      timeAnchor += skip_seconds
      trueList[sorted_list[counter][0]] = tree[sorted_list[counter][0]]
    }
    counter += 1
  }
  return trueList
}
export const filter_funcs = {
  no_filter_func: no_filter_func,
  view_filter_func: view_filter_func,
  flagged_filter_func: flagged_filter_func
}

function generate_sidebar_filelist (
  tree,
  sidebar_list_datetime_sorted,
  side,
  sort_func,
  // label_func,
  filter_func,
  selected_file,
  skip_seconds
) {
  const chosen_images = skip_filter_func(
    sidebar_list_datetime_sorted,
    tree,
    skip_seconds
  )
  return Object.entries(chosen_images)
    .filter(
      ([filename, metadata]) =>
        filter_func([filename, metadata]) ||
        (selected_file &&
          selected_file.side === side &&
          selected_file.file === filename)
    )
    .sort(sort_func)
}
function generate_sidebar_filelists (state) {
  let newState = {
    ...state,
    sidebar_list_datetime_sorted: {
      left: generate_sidebar_filelist(
        state.tree.left,
        null,
        'left',
        datetime_sort_func,
        no_filter_func,
        state.selected_file,
        0
      ),
      right: generate_sidebar_filelist(
        state.tree.right,
        null,
        'right',
        datetime_sort_func,
        no_filter_func,
        state.selected_file,
        0
      )
    }
  }
  newState = {
    ...newState,
    sidebar_list: {
      left: generate_sidebar_filelist(
        newState.tree.left,
        newState.sidebar_list_datetime_sorted.left,
        'left',
        sort_funcs[state.settings.sidebar_sort_function],
        filter_funcs[state.settings.sidebar_filter_function],
        state.selected_file,
        state.settings.skip_seconds
      ),
      right: generate_sidebar_filelist(
        newState.tree.right,
        newState.sidebar_list_datetime_sorted.right,
        'right',
        sort_funcs[state.settings.sidebar_sort_function],
        filter_funcs[state.settings.sidebar_filter_function],
        state.selected_file,
        state.settings.skip_seconds
      )
    }
  }
  return newState
}

function getAdjacentIndex (
  getNext,
  currentSidebarFolderContents,
  otherSidebarFolderContents,
  selected_file,
  currentMetadata
) {
  // console.log(
  //   currentSidebarFolderContents,
  //   otherSidebarFolderContents,
  //   selected_file,
  //   currentMetadata
  // )
  return Array.findFirstOrLastIndex(
    otherSidebarFolderContents,
    getNext,
    ([filename, metadata]) =>
      getNext
        ? metadata.timestamp > currentMetadata.timestamp
        : metadata.timestamp < currentMetadata.timestamp
  )
}

function getAdjacentOrOtherside (
  getNext,
  currentSidebarFolderContents,
  otherSidebarFolderContents,
  selected_file,
  currentIndex
) {
  let nextOrPrevFile = null
  const currentMetadata = currentSidebarFolderContents[currentIndex][1]
  const [nextFilename, nextMetadata] = currentSidebarFolderContents[
    currentIndex + (getNext ? 1 : -1)
  ] || [null, null]
  const nextOtherSideIndex = getAdjacentIndex(
    getNext,
    currentSidebarFolderContents,
    otherSidebarFolderContents,
    selected_file,
    currentMetadata
  )
  const [otherSideFilename, otherSideMetadata] = otherSidebarFolderContents[
    nextOtherSideIndex
  ] || [null, null]
  if (otherSideMetadata) {
    if (
      nextMetadata &&
      (getNext
        ? nextMetadata.timestamp < otherSideMetadata.timestamp
        : nextMetadata.timestamp > otherSideMetadata.timestamp)
    ) {
      nextOrPrevFile = { side: selected_file.side, file: nextFilename }
    } else {
      nextOrPrevFile = {
        side: otherSide(selected_file.side),
        file: otherSideFilename
      }
    }
  } else {
    if (nextMetadata) {
      nextOrPrevFile = { side: selected_file.side, file: nextFilename }
    }
  }
  return nextOrPrevFile
}
function getClosestOtherside (
  currentSidebarFolderContents,
  otherSidebarFolderContents,
  selected_file
) {
  let otherSideFile = null
  const currentIndex = currentSidebarFolderContents.findIndex(
    ([filename, metadata]) => filename === selected_file.file
  )
  if (currentIndex !== -1) {
    const currentMetadata = currentSidebarFolderContents[currentIndex][1]

    const nextOtherSideIndex = getAdjacentIndex(
      true,
      currentSidebarFolderContents,
      otherSidebarFolderContents,
      selected_file,
      currentMetadata
    )
    const prevOtherSideIndex = getAdjacentIndex(
      false,
      currentSidebarFolderContents,
      otherSidebarFolderContents,
      selected_file,
      currentMetadata
    )

    let otherSideMetadata = null
    let otherSideIndex = -1
    if (nextOtherSideIndex !== -1) {
      otherSideMetadata = otherSidebarFolderContents[nextOtherSideIndex][1]
      otherSideIndex = nextOtherSideIndex
    }
    if (prevOtherSideIndex !== -1) {
      const _otherSideMetadata =
        otherSidebarFolderContents[prevOtherSideIndex][1]
      if (
        Math.abs(_otherSideMetadata.timestamp - currentMetadata.timestamp) <
        Math.abs(otherSideMetadata.timestamp - currentMetadata.timestamp)
      ) {
        otherSideMetadata = _otherSideMetadata
        otherSideIndex = prevOtherSideIndex
      }
    }
    if (otherSideMetadata !== null) {
      otherSideFile = {
        side: otherSide(selected_file.side),
        file: otherSidebarFolderContents[otherSideIndex][0]
      }
    }
  }
  return otherSideFile
}

function get_next_prev_on_same_side (
  selected_file,
  currentSidebarFolderContents,
  currentIndex
) {
  let nextFile = null
  let prevFile = null
  const nextIndex = clip(
    currentIndex + 1,
    0,
    currentSidebarFolderContents.length - 1
  )
  const prevIndex = clip(
    currentIndex - 1,
    0,
    currentSidebarFolderContents.length - 1
  )
  if (nextIndex !== currentIndex) {
    const [file, _] = currentSidebarFolderContents[nextIndex] // eslint-disable-line
    nextFile = { file, side: selected_file.side }
  }
  if (prevIndex !== currentIndex) {
    const [file, _] = currentSidebarFolderContents[prevIndex] // eslint-disable-line
    prevFile = { file, side: selected_file.side }
  }
  return [prevFile, nextFile]
}

function get_next_prev_images (state, selected_file) {
  let newAnchors = state.anchors
  let nextFile = null
  let prevFile = null
  if (selected_file !== null) {
    if (
      state.settings.cycle_mode === 'stay_on_side' ||
      state.settings.cycle_mode === 'filebrowser'
    ) {
      const currentSidebarFolderContents =
        state.sidebar_list[selected_file.side]
      const currentIndex = currentSidebarFolderContents.findIndex(
        ([filename, metadata]) => filename === selected_file.file
      )
      if (currentIndex !== -1) {
        ;[prevFile, nextFile] = get_next_prev_on_same_side(
          selected_file,
          currentSidebarFolderContents,
          currentIndex
        )
      }
    } else if (state.settings.cycle_mode === 'alternate_sides') {
      const currentSidebarFolderContents =
        state.sidebar_list[selected_file.side]
      const otherSidebarFolderContents =
        state.sidebar_list[otherSide(selected_file.side)]
      const currentIndex = currentSidebarFolderContents.findIndex(
        ([filename, metadata]) => filename === selected_file.file
      )
      if (currentIndex !== -1) {
        nextFile = getAdjacentOrOtherside(
          true,
          currentSidebarFolderContents,
          otherSidebarFolderContents,
          selected_file,
          currentIndex
        )
        prevFile = getAdjacentOrOtherside(
          false,
          currentSidebarFolderContents,
          otherSidebarFolderContents,
          selected_file,
          currentIndex
        )
      }
    } else if (state.settings.cycle_mode === 'smart_cycle') {
      if (state.anchors[otherSide(selected_file.side)] === null) {
        const currentSidebarFolderContents =
          state.sidebar_list[selected_file.side]
        const currentIndex = currentSidebarFolderContents.findIndex(
          ([filename, metadata]) => filename === selected_file.file
        )
        if (currentIndex !== -1) {
          ;[prevFile, nextFile] = get_next_prev_on_same_side(
            selected_file,
            currentSidebarFolderContents,
            currentIndex
          )
        }
      } else {
        const currentSidebarFolderContents =
          state.sidebar_list[selected_file.side]
        const otherSidebarFolderContents =
          state.sidebar_list[otherSide(selected_file.side)]
        const currentIndex = currentSidebarFolderContents.findIndex(
          ([filename, metadata]) => filename === selected_file.file
        )
        if (currentIndex !== -1) {
          const currentMetadata = currentSidebarFolderContents[currentIndex][1]

          // const thisSideAnchor = state.anchors[selected_file.side]
          const otherSideAnchor = state.anchors[otherSide(selected_file.side)]
          const otherSideIndex = otherSidebarFolderContents.findIndex(
            ([filename, metadata]) => filename === otherSideAnchor.file
          )
          if (otherSideIndex !== -1) {
            ;[prevFile, nextFile] = get_next_prev_on_same_side(
              selected_file,
              currentSidebarFolderContents,
              currentIndex
            )

            const otherSideAnchorMetadata =
              otherSidebarFolderContents[otherSideIndex][1]
            if (
              currentMetadata.timestamp - otherSideAnchorMetadata.timestamp >
                state.settings.smartCycleMaxTimeDiff ||
              currentIndex === currentSidebarFolderContents.length - 1
            ) {
              // Keep nextFile as is if getAnchorAdjacentFiles returns null
              nextFile =
                getAnchorAdjacentFiles(state, otherSideAnchor)[1] || nextFile
            }
            if (prevFile) {
              const prevMetadata = state.tree[prevFile.side][prevFile.file]
              if (
                otherSideIndex > 0 &&
                otherSideAnchorMetadata.timestamp - prevMetadata.timestamp >
                  state.settings.smartCycleMaxTimeDiff
              ) {
                prevFile = otherSideAnchor
              }
            } else {
              if (otherSideAnchor) {
                prevFile = otherSideAnchor
              }
            }
          } else {
            // What now? (anchor on other side does not exist anymore)
          }
        }
      }
    }
  }
  return [prevFile, nextFile, newAnchors]
}
function generate_next_prev_images (state) {
  const [newPrevFile, newNextFile, newAnchors] = get_next_prev_images(
    state,
    state.selected_file
  )
  return {
    ...state,
    anchors: newAnchors,
    prev_file: newPrevFile,
    next_file: newNextFile
  }
}
function generate_smart_anchor_images (state) {
  if (
    state.settings.cycle_mode !== 'smart_cycle' ||
    state.selected_file === null
  ) {
    return state
  }
  const selected_file = state.selected_file
  const currentSidebarFolderContents = state.sidebar_list[selected_file.side]
  const otherSidebarFolderContents =
    state.sidebar_list[otherSide(selected_file.side)]
  const currentIndex = currentSidebarFolderContents.findIndex(
    ([filename, metadata]) => filename === selected_file.file
  )
  const anchors = { left: null, right: null }
  anchors[selected_file.side] = selected_file
  if (currentIndex !== -1) {
    const currentMetadata = currentSidebarFolderContents[currentIndex][1]
    const otherSideIndex = getAdjacentIndex(
      true,
      currentSidebarFolderContents,
      otherSidebarFolderContents,
      selected_file,
      currentMetadata
    )
    if (otherSideIndex !== -1) {
      const otherSideFilename = otherSidebarFolderContents[otherSideIndex][0]
      anchors[otherSide(selected_file.side)] = {
        side: otherSide(selected_file.side),
        file: otherSideFilename
      }
    }
    return { ...state, anchors: anchors }
  }
  return state
}
function getAnchorAdjacentFiles (state, selected_file) {
  let nextFile = null
  let prevFile = null
  const currentSidebarFolderContents = state.sidebar_list[selected_file.side]
  const currentIndex = currentSidebarFolderContents.findIndex(
    ([filename, metadata]) => filename === selected_file.file
  )
  if (currentIndex !== -1) {
    ;[prevFile, nextFile] = get_next_prev_on_same_side(
      selected_file,
      currentSidebarFolderContents,
      currentIndex
    )
  }
  return [prevFile, nextFile]
}

function addExtraInfo (state, files) {
  Object.entries(files).forEach(([file, metadata]) => {
    try {
      const subsec = metadata.exif.exif.SubSecTimeOriginal || 0.0
      metadata.timeLabel = new Date(metadata.exif.exif.DateTimeOriginal).toLocaleTimeString()
      metadata.timestamp = Date.parse(metadata.exif.exif.DateTimeOriginal) + parseFloat('0.' + subsec)
    } catch (err) {
      metadata.timeLabel = file
      metadata.timestamp = Infinity
    }
    const settings = state.settings
    metadata.rawProbWholeImage = -1
    metadata.probabilityWholeImage = -1
    metadata.rawProbBboxes = -1
    metadata.probabilityBboxes = -1

    metadata.image_probabilities = []
    metadata.image_probabilities_raw = []
    Object.entries(settings.algos.classification).forEach(
      ([algo, { normalization_threshold_logit }]) => {
        if (metadata.algoResults.hasOwnProperty(algo)) {
          const p = metadata.algoResults[algo].imageClassificationConfidence
          metadata.image_probabilities_raw.push(p)
          metadata.image_probabilities.push(
            remap_threshold(p, normalization_threshold_logit)
          )
        } else {
          metadata.image_probabilities_raw.push(-1)
          metadata.image_probabilities.push(-1)
        }
      }
    )
    if (metadata.image_probabilities.length > 0) {
      metadata.rawProbWholeImage = Math.max(...metadata.image_probabilities_raw)
      metadata.probabilityWholeImage = Math.max(...metadata.image_probabilities)
    }

    metadata.bbox_probabilities = []
    metadata.bbox_probabilities_raw = []
    Object.entries(settings.algos.detection).forEach(
      ([algo, { normalization_threshold, threshold }]) => {
        if (metadata.algoResults.hasOwnProperty(algo)) {
          const probabilities = metadata.algoResults[algo].objectDetections[2]

          const p = Math.max(0, ...probabilities.filter(p => p >= threshold))

          metadata.bbox_probabilities_raw.push(p)
          metadata.bbox_probabilities.push(
            remap_threshold(p, normalization_threshold)
          )
        } else {
          metadata.bbox_probabilities_raw.push(-1)
          metadata.bbox_probabilities.push(-1)
        }
      }
    )
    if (metadata.bbox_probabilities.length > 0) {
      metadata.rawProbBboxes = Math.max(...metadata.bbox_probabilities_raw)
      metadata.probabilityBboxes = Math.max(...metadata.bbox_probabilities)
    }
    
    metadata.tracked_bbox_probabilities = []
    metadata.tracked_bbox_probabilities_raw = []
    Object.entries(settings.algos.tracking).forEach(
      ([algo, { normalization_threshold, threshold }]) => {
        if (metadata.algoResults.hasOwnProperty(algo)) {
          var raw_probabilities = []
          Object.entries(metadata.algoResults[algo].tracks).forEach( 
          ( track ) => {
            raw_probabilities.push(track[1].existanceProb)
          });
          const p = Math.max(0, ...raw_probabilities.filter(p => p >= threshold))

          metadata.tracked_bbox_probabilities_raw.push(p)
          metadata.tracked_bbox_probabilities.push(
            remap_threshold(p, normalization_threshold)
          )
        } else {
          metadata.tracked_bbox_probabilities_raw.push(-1)
          metadata.tracked_bbox_probabilities.push(-1)
        }
      }
    )
    if (metadata.tracked_bbox_probabilities.length > 0) {
      metadata.rawProbTracks = Math.max(...metadata.tracked_bbox_probabilities_raw)
      metadata.probabilityTracks = Math.max(...metadata.tracked_bbox_probabilities)
    }

    metadata.rawProb = Math.max(
      metadata.rawProbWholeImage,
      metadata.rawProbTracks === -1 ? metadata.rawProbBboxes : metadata.rawProbTracks
    )
    metadata.probability = Math.max(
      metadata.probabilityWholeImage,
      metadata.probabilityTracks === -1 ? metadata.probabilityBboxes : metadata.probabilityTracks
    )
  })
}

function extractDate (state) {
  const keys = Object.keys(state.tree.left)
  if (state.date === null && keys.length > 0) {
    try {
      const date = new Date(state.tree.left[keys[0]].exif.exif.DateTimeOriginal).toLocaleDateString()
      return { ...state, date: date }
    } catch (err) {
      return { ...state, date: null }
    }
  }
  return state
}

const images = (
  state = {
    subfolder: null,
    tree: { left: {}, right: {} },
    anchors: { left: {}, right: {} },
    sidebar_list: { left: [], right: [] },
    sidebar_list_datetime_sorted: { left: [], right: [] },
    selected_file: null,
    next_file: null,
    prev_file: null,
    newAlgoData_for_selected_image_counter: 0
  },
  action
) => {
  !action.type.startsWith('@') && console.log(action.type)
  switch (action.type) {
    case 'SELECT_SUBFOLDER':
      return {
        ...state,
        subfolder: action.subfolder,
        date: null,
        sidebar_list: { left: [], right: [] },
        selected_file: null,
        next_file: null,
        prev_file: null,
        sidebar_list_datetime_sorted: { left: [], right: [] },
        anchors: { left: {}, right: {} }
      }

    case 'RECEIVE_LEFT_RIGHT_ALL_FILES': {
      addExtraInfo(state, action.left)
      addExtraInfo(state, action.right)
      let newState = {
        ...state,
        tree: { left: action.left, right: action.right }
      }
      return generate_next_prev_images(
        generate_sidebar_filelists(extractDate(newState))
      )
    }

    case 'RECEIVE_CHANGED_FILES': {
      if (action.subfolder !== state.subfolder) {
        return state
      }
      addExtraInfo(state, action.newFileData)
      addExtraInfo(state, action.newAlgoData)
      const newState = {
        ...state,
        tree: {
          ...state.tree,
          [action.side]: {
            ...Object.filter(
              state.tree[action.side],
              (file, struct) => action.removedFiles.indexOf(file) === -1
            ),
            ...action.newFileData,
            ...action.newAlgoData
          }
        }
      }
      if (
        state.selected_file &&
        state.selected_file.side === action.side &&
        action.newAlgoData.hasOwnProperty(state.selected_file.file)
      ) {
        newState.newAlgoData_for_selected_image_counter += 1
      }
      return generate_next_prev_images(
        generate_sidebar_filelists(extractDate(newState))
      )
    }

    case 'RECEIVE_CHANGED_METADATA': {
      if (action.subfolder !== state.subfolder) {
        return state
      }
      const newState = {
        ...state,
        tree: {
          ...state.tree,
          [action.side]: {
            ...state.tree[action.side],
            [action.file]: {
              ...state.tree[action.side][action.file],
              manual: action.changedData
            }
          }
        }
      }
      return generate_next_prev_images(generate_sidebar_filelists(newState))
    }

    case 'SELECT_FILE':
      if (
        !state.selected_file ||
        state.selected_file.side !== action.side ||
        state.selected_file.file !== action.file
      ) {
        const newSelectedFile = { side: action.side, file: action.file }
        const newState = { ...state, selected_file: newSelectedFile }
        return generate_next_prev_images(generate_smart_anchor_images(newState))
      }
      return state

    case 'SET_SORT_FUNC': {
      let newState = {
        ...state,
        settings: {
          ...state.settings,
          sidebar_sort_function: action.sort_func
        }
      }
      if (action.sort_func !== 'datetime_sort_func') {
        newState = {
          ...newState,
          settings: { ...newState.settings, cycle_mode: 'stay_on_side' }
        }
      }
      return generate_next_prev_images(generate_sidebar_filelists(newState))
    }
    case 'SET_SKIP_SECONDS': {
      const newState = {
        ...state,
        settings: {
          ...state.settings,
          skip_seconds: parseInt(action.skip_seconds)
        }
      }
      return generate_next_prev_images(generate_sidebar_filelists(newState))
    }
    case 'SET_FILTER_FUNC': {
      const newState = {
        ...state,
        settings: {
          ...state.settings,
          sidebar_filter_function: action.filter_func
        }
      }
      return generate_next_prev_images(generate_sidebar_filelists(newState))
    }
    case 'SET_CYCLE_MODE': {
      const newState = {
        ...state,
        settings: { ...state.settings, cycle_mode: action.cycle_mode }
      }
      return generate_next_prev_images(newState)
    }

    case 'GO_TO_OTHER_SIDE': {
      if (state.selected_file !== null) {
        const currentSidebarFolderContents =
          state.sidebar_list[state.selected_file.side]
        const otherSidebarFolderContents =
          state.sidebar_list[otherSide(state.selected_file.side)]
        const otherSideFile = getClosestOtherside(
          currentSidebarFolderContents,
          otherSidebarFolderContents,
          state.selected_file
        )
        if (otherSideFile) {
          const newState = { ...state, selected_file: otherSideFile }
          return generate_next_prev_images(
            generate_smart_anchor_images(newState)
          )
        }
      }
      return state
    }

    case 'NEXT_IMAGE':
    case 'PREV_IMAGE': {
      if (state.selected_file) {
        const newFile =
          action.type === 'NEXT_IMAGE' ? state.next_file : state.prev_file
        if (newFile !== null) {
          let newState = { ...state, selected_file: newFile }
          if (state.settings.cycle_mode === 'smart_cycle') {
            newState = {
              ...newState,
              anchors: { ...newState.anchors, [newFile.side]: newFile }
            }
            if (state.selected_file.side === newFile.side) {
            } else if (action.type === 'PREV_IMAGE') {
              if (state.anchors[state.selected_file.side]) {
                const anchorAdjacentPrevFile = getAnchorAdjacentFiles(
                  state,
                  state.anchors[state.selected_file.side]
                )[0]
                if (anchorAdjacentPrevFile !== null) {
                  newState = {
                    ...newState,
                    anchors: {
                      ...newState.anchors,
                      [anchorAdjacentPrevFile.side]: anchorAdjacentPrevFile
                    }
                  }
                }
                // const anchorAdjacentFile =
                //   action.type === 'NEXT_IMAGE'
                //     ? anchorAdjacentNextFile
                //     : anchorAdjacentPrevFile
                // if (anchorAdjacentFile !== null) {
                //   newState = {
                //     ...newState,
                //     anchors: {
                //       ...newState.anchors,
                //       [anchorAdjacentFile.side]: anchorAdjacentFile
                //     }
                //   }
                // }
              }
            }
          }
          return generate_next_prev_images(newState)
        }
      }
      return state
    }
    case 'SET_SETTINGS':
      if (action.settings.sidebar_sort_function !== 'datetime_sort_func') {
        action.settings.cycle_mode = 'stay_on_side'
      }
      // Compute logit thresholds with the inverse logistic function for later normalization of the algorithm outputs,
      // where the threshold is mapped to 50% (for the sidebar indicator and bounding box colors)
      const algos = action.settings.algos
      ;[algos.detection, algos.classification].forEach(algos => {
        Object.entries(algos).forEach(([algo, algo_settings]) => {
          let normalization_threshold =
            algo_settings.hasOwnProperty('normalization_threshold') &&
            algo_settings.normalization_threshold !== -1
              ? algo_settings.normalization_threshold
              : 0.5
          algo_settings.normalization_threshold_logit = -Math.log(
            normalization_threshold / (1 - normalization_threshold)
          )
        })
      })
      return generate_next_prev_images({
        ...state,
        settings: action.settings
      })
    default:
      return state
  }
}

export default images
