import { useEffect } from 'react'
import { connect, useDispatch } from 'react-redux'
import { connectToBackend } from './reducers/rootReducer.js'
import Home from './Home'
import FolderWatch from './FolderWatch'
import Debug from './Debug'
import './App.css'

function App (props) {
  const dispatch = useDispatch()
  const isDebug = window.location.pathname === '/debug'
  useEffect(() => {
    if (!isDebug) {
      // The if is a little hacky. It prevents the socket connection from rootReducer to be initiated on the debug page
      dispatch(connectToBackend())
    }
  }, [dispatch, isDebug])

  if (!isDebug && props.connected === 'not_yet_connected') {
    return <div>Connecting to backend...</div>
  } else if (!isDebug && props.connected === 'lost_connection') {
    return <div>Not connected to backend. Trying to reconnect...</div>
  } if (isDebug) {
    return <Debug />
  }
  return (
    <div className='App'>
      {props.subfolder === null && <Home />}
      {props.subfolder !== null && <FolderWatch />}
    </div>
  )
}

const mapDispatchToProps = {}
const mapStateToProps = state => ({
  subfolder: state.images.subfolder,
  connected: state.connection.connected
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
