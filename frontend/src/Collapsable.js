import { useState } from 'react'
import './Collapsable.css'

function Collapsable (props) {
  const [collapsed, setCollapsed] = useState(props.collapsed || false)
  return (
    <div className='Collapsable'>
      <header onClick={() => setCollapsed(!collapsed)}>
        <main>{props.title}</main>
        <aside>{collapsed ? <span>&#9656;</span> : <span>&#9662;</span>}</aside>
      </header>
      {!collapsed && <main>{props.children}</main>}
    </div>
  )
}

export default Collapsable
