import React from 'react'
import './PopupWindow.css'

export default class PopupWindow extends React.Component {
  render () {
    return (
      <div className='PopupWindow'>
        <div className='background' onClick={this.props.onClose} />
        {this.props.children}
      </div>
    )
  }
}
