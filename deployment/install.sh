#!/bin/bash

# check if docker is available
if ! command -v docker &> /dev/null
then
    echo "docker could not be found..."
    exit
fi


sed -i 's@./backend/data@/home/searchwing/flightdata@;' docker-compose-prod.yml

su -c 'docker compose -f docker-compose-prod.yml build' searchwing

# install services
sudo cp deployment/services/* /lib/systemd/system
sudo systemctl enable searchwing-gui
sudo systemctl start searchwing-gui
