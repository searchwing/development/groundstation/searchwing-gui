
# Deployment

1. Run `install.sh` from repo root directory as root:

```bash
sudo deployment/install.sh
```

2. Service `searchwing-gui` for autostart is now enabled and started using.