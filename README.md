
# Searchwing GUI

* A tool to visualize flight image data and detections from our [boatdetection software](https://gitlab.com/searchwing/development/payloads/boatdetector-offline/).
* The JPEG images include metadata like drone position (regular exif-fields) and additional infos like covered ground area (as json in the exif-comment).
* Detectors provide 2d boat detections in image coordinates and a tracking result in geocoordinates.
* The tool expects the flightdata in a [specific format](https://wiki.searchwing.org/en/home/using-the-drone/checklists/after-flight-checklist#h-8-upload-data-to-searchwing-data-server) and can handle continuosly added images to this folder structure (e.g. when downloading the 20GB of images from a single flight).

## Install dependencies
(You can skip installation if you start the GUI with docker-compose)

- Install nodejs, e.g. with

```bash
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs
```

## Setup Datafolders

* Set your flight data folder in /backend/config.js : `datadir: '/absolute/path/to/data/dir'`
* Our drones use two cameras (left&right camera) - Set your camera left and right prefix folder names in /backend/config.js : ` left: 'cam-l', right: 'cam-r'`

## Import Data

* Download example data to default flight data folder `/backend/data` :
```bash
bash download_example_images.sh
```

* OR: Follow [instructions from our wiki](https://wiki.searchwing.org/en/home/Manual/using-the-drone/after-flight) on how to fill mission data into the gui

## Start GUI
### Docker compose
Start the GUI:
```bash
docker compose up
```
In the file `docker-compose.yml` line 22 the bind mount is defined. Hence, if you want to read data from a different directory than `backend/data` change the line to:

`- <your-data-folder>:/app/data`

Note that in case you have changed some code (also the options in `/backend/config.js`), you should rebuild the docker container
```bash
docker compose up --build
```

### Manual
### Run backend

```bash
cd backend
npm install
node index.js
```
### Run frontend

```bash
cd frontend
npm install
npm start
```

- Go to http://localhost:3000

## Release for production

- Push build containers to the container registry: https://gitlab.com/searchwing/development/groundstation/searchwing-gui/container_registry